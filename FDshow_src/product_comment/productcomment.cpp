#include "productcomment.h"
#include "../public/Log.h"
#include <sstream>
#include <mysql++.h>
#include <exceptions.h>
#include "../public/typedef.h"
#include "../public/DBConnectPool.h"
#include "../public/datachange.h"
#include "../public/protocol.h"
#include "../public/ThreadBase.h"
#include "../json/json.h"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "fcgi_stdio.h"

using namespace std;

#define RECV_BUF_SIZE 1024
#define memsize 3096

#define QUERY_DIS_SQL "SELECT  a.vch_userid as userid,b.vch_username as username,b.vch_url as url,1 as flag,b.vch_gender as gender, \
cast(6371004*2*ASIN(SQRT(POWER(SIN((%s - a.vch_latitude)*pi()/360),2) + COS(%s*pi()/180)*COS(a.vch_latitude*pi()/180)*POWER(SIN((%s - a.vch_longitude)*pi()/360),2))) as UNSIGNED) as distance \
FROM  t_login_info a,t_personal_info b  \
WHERE a.vch_userid = b.vch_userid and a.int_flag = 1 \
and a.vch_userid in(select vch_friend_id from t_friend_info where vch_userid = '%s' and int_sheild = 1) \
and a.vch_longitude between %s-100000*180/abs(cos(%s*pi()/180)*111044.736)/pi() and %s+100000*180/abs(cos(%s*pi()/180)*111044.736)/pi() \
and a.vch_latitude between %s-(100000/111044.736)*180/pi() and %s+(100000/111044.736)*180/pi() \
having distance < 100000 \
union \
SELECT  a.vch_userid, b.vch_username as username,b.vch_url as url,0 as flag,b.vch_gender as gender, \
cast(6371004*2*ASIN(SQRT(POWER(SIN((%s - a.vch_latitude)*pi()/360),2) + COS(%s*pi()/180)*COS(a.vch_latitude*pi()/180)*POWER(SIN((%s - a.vch_longitude)*pi()/360),2))) as UNSIGNED) as distance \
FROM  t_login_info a,t_personal_info b,t_step_record_share c \
WHERE a.vch_userid = b.vch_userid and a.int_flag = 1 \
and a.vch_userid not in(select vch_friend_id from t_friend_info where vch_userid = '%s' and int_sheild = 1) \
and a.vch_longitude between %s-100000*180/abs(cos(%s*pi()/180)*111044.736)/pi() and %s+100000*180/abs(cos(%s*pi()/180)*111044.736)/pi() \
and a.vch_latitude between %s-(100000/111044.736)*180/pi() and %s+(100000/111044.736)*180/pi() \
having distance < 100000 ORDER BY distance limit 50;"



pthread_mutex_t FDSocialTeam::m_Mutex = PTHREAD_MUTEX_INITIALIZER;

FDSocialTeam::FDSocialTeam(){
	initialize();
}
int FDSocialTeam::initialize() {
	FCGX_InitRequest(&m_Request, 0, 0);
	retcode = 401;
	return 0;
}

string FDSocialTeam::getEnv(const string& param) {
	char* val = FCGX_GetParam(param.c_str(), m_Request.envp);
	if (val == NULL) {
		return "";
	}
	return val;
}

void FDSocialTeam::run() {

	while (!m_Shutdown) {
		Json::Value GenericRetString,ShareContentString,CommentString,FriendShipRank;
		pthread_mutex_lock(&m_Mutex);
		int ret = FCGX_Accept_r(&m_Request);
		pthread_mutex_unlock(&m_Mutex);

		if (ret < 0) {
			SVC_LOG((LM_ERROR, " FCG_RUN:FCGX_Accept_r failed ret=%d",ret));
			continue;
		}
		string value = getEnv("REQUEST_METHOD");
		if (0 == strcasecmp(value.c_str(), "post")) { //deal the method post
			char buf[RECV_BUF_SIZE + 1] = { 0 };
			long length = strtoul(getEnv("CONTENT_LENGTH").c_str(), NULL, 10);
			if (length == 0) { //if length equal 0 :maybe error
				SVC_LOG((LM_ERROR, " FCG_RUN:Http header Content-Length invalid:content-length=%d", length));
				retcode = 402;
			} else {
				FCGX_GetStr(buf, sizeof(buf), m_Request.in);
				SVC_LOG((LM_INFO, " FCG_RUN:msg=begin to read Http content:length=%u ", length));
				//parse the content
				SVC_LOG((LM_INFO,"Test:%s",buf));
				parseQueryString(buf);//parse the string and resort to map
				
				if (0 == m_UrlParams.size()) {
					SVC_LOG((LM_INFO, " FCG_RUN: msg=parse parm erron!"));
					retcode = 402;
				}
				if(!HandleSql(FriendShipRank,ShareContentString,CommentString)){//check parameters and operator the database
					SVC_LOG((LM_INFO, " FCG_RUN:parameters error or operator database error!"));
				}
			}
		} else {
			retcode = 405;
		}
		//return to client
		Response(GenericRetString,FriendShipRank,ShareContentString,CommentString);
	}
}

void FDSocialTeam::parseQueryString(const string& str) {
	size_t begin = 0, pos;
	m_UrlParams.clear();
	do {
		pos = str.find('=', begin);
		if (pos != string::npos) {
			string key = str.substr(begin, pos - begin);
			begin = pos + 1;
			pos = str.find('&', begin);
			if (pos != string::npos) {
				m_UrlParams[key] = URLDecode(str.substr(begin, pos - begin));
				begin = pos + 1;
			} else {
				m_UrlParams[key] = URLDecode(str.substr(begin));
			}
		}
	} while (pos != string::npos);
}

bool FDSocialTeam::HandleSql(Json::Value& FriendShipRank,Json::Value& GenericRetString,Json::Value& CommentString) {
	StringMap::iterator pos;
	ConnPool * mypool = ConnPool::GetInstance();
	mysqlpp::Connection *conn = mypool->GetConnection();

	string userid;
	retcode = 401;
	char * sqlstate = new char[memsize];
	if (NULL == sqlstate) {
		SVC_LOG((LM_ERROR, "HandleSql:malloc memery failed! "));
		retcode = 404; //server operator error
		return false;
	}

	int options; //options;
	pos = m_UrlParams.find("options");
	if (pos != m_UrlParams.end()) {
		SVC_LOG((LM_INFO, " options =%s",pos->second.c_str()));
		options = atoi(pos->second.c_str());
	}

	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
    SVC_LOG((LM_INFO,"HERE HRER HERE"));
	switch (options){
	case 0:{
		//initialize the friendship.include the latest sport information
		if(init_friendship(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "initialize friendship operator successful!"));
		}
		break;
	}
	case 1:{
		//info's share setup
		if(InfoShare_Setup(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "infoshare setup operator successful!"));
		}
		break;
	}
	case 2:{
		//find the neighbour user
		if(FindNeigbour(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "find the neighour user operator successful!"));
		}
		break;
	}
	case 3:{
		//find the certain user userid=371058455&friendid=memsize000/email/缂佹垵鐣鹃幍瀣簚閻劍鍩沵obil_number
		if(FindCertainUser(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "find certain user operator successful!"));
		}
		break;
	}
	case 4:{
		//add friend
		if(AddDelFriend(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "add friend operator successful!"));
		}
		break;
	}
	case 5:{
		//initialize social friendship
		//select a.vch_username,a.vch_url,b.* from t_personal_info a,t_step_record_share b where a.vch_userid in(select vch_friend_id from t_friend_info  where vch_userid =  memsize00) and a.vch_userid = b.vch_userid;
		if(InitSocialFriendship(conn,sqlstate,FriendShipRank,GenericRetString,CommentString)){
			SVC_LOG((LM_INFO, "initialize social friendship operator successful!"));
		}
		break;
	}
	case 6:{
		//add comments 
		//options=6&recordid=xxxx&shareuserid=371058455&commentid=memsize000&symbol=1&comment= 閳ユ壕锟�comefrom=0&replayeid=xx
		if(AddComment(conn,sqlstate,GenericRetString,CommentString)){
			SVC_LOG((LM_INFO, "add comment operator successful!"));
		}
		break;
	}
	case 7:{//delete the information of the specialize userid
		if(DeleteComment(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "delete comment operator successful!"));
		}
		break;
	}
	case 8:{
		if(SpecialComment(conn,sqlstate,GenericRetString,CommentString)){
			SVC_LOG((LM_INFO, "return special comment operator successful!"));
		}
		break;
	}
	case 9:{
		if(StepRank(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "return step ranking operator successful!"));
		}
		break;
	}
	case 10:{
		if(RecordShare(conn,sqlstate,GenericRetString)){
			SVC_LOG((LM_INFO, "record the user share successful!"));
		}
		break;
	}
	default:{
		retcode = 402;
		break;
	}
	}
	END:
	//all is done need release the resource
	if(NULL!=conn){
		mypool->ReleaseConnection(conn);
	}
	if (NULL != sqlstate){
		delete sqlstate;
		sqlstate = NULL;
	}
	//return the result of function
	if(retcode%100 == 1 )
		return true;
	else
		return false;
}

bool FDSocialTeam::init_friendship(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
			string userid,select;
			StringMap::iterator pos;
			mysqlpp::Query query(conn,true,NULL);
			pos = m_UrlParams.find("userid");
			if (pos != m_UrlParams.end()) {
				userid = pos->second;
			}
			pos = m_UrlParams.find("select");
			if (pos != m_UrlParams.end()) {
				select = pos->second;
			}
			if(userid.empty()||select.empty()){
				retcode = 402;
				return false;
			}
			memset(sqlstate, '\0', memsize); //TODO
			if(atoi(select.c_str())==1){
				snprintf(sqlstate, memsize,"select vch_userid as userid,vch_username as username,vch_url as url from t_personal_info where vch_userid in (select vch_friend_id from t_friend_info where vch_userid = '%s' and int_sheild = 1)",userid.c_str());
			}else if(atoi(select.c_str())==2){ //my fans
				snprintf(sqlstate,memsize,"select a.vch_userid as userid,a.vch_username as username,a.vch_url as url,1 as flag,a.vch_gender as gender from t_personal_info a,(select vch_userid from t_friend_info where vch_friend_id ='%s' and vch_userid <> '%s' and int_sheild = 1) b where a.vch_userid = b.vch_userid and b.vch_userid in(select vch_friend_id from t_friend_info where vch_friend_id <>'%s' and vch_userid = '%s' and int_sheild = 1) union select a.vch_userid as userid,a.vch_username as username,a.vch_url as url,0 as flag,a.vch_gender as gender from t_personal_info a,(select vch_userid from t_friend_info where vch_friend_id ='%s' and vch_userid <> '%s' and int_sheild = 1) b  where a.vch_userid = b.vch_userid and b.vch_userid not in(select vch_friend_id from t_friend_info where vch_friend_id <>'%s' and vch_userid = '%s' and int_sheild = 1) order by username;",userid.c_str(),userid.c_str(),userid.c_str(),userid.c_str(),userid.c_str(),userid.c_str(),userid.c_str(),userid.c_str());
			}else{
				retcode = 402;
				return false;
			}
			SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
			try{
				query.reset();
		        query.exec("set names utf8");
				query = conn->query(sqlstate);
				mysqlpp::StoreQueryResult FriendRes = query.store();
                if(!FriendRes.empty()){
    				GenericRetString = ResToJson(FriendRes);
                }
				SVC_LOG((LM_INFO, "HandleSql:sql operator end! "));
				retcode = 401;
			}catch(mysqlpp::Exception&){
				SVC_LOG((LM_ERROR, "HandleSql:sql operator failed! "));
				retcode = 404;
				return false;
			}
			return true;
		}

bool FDSocialTeam::InfoShare_Setup(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string userid,share;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("share");
	if (pos != m_UrlParams.end()) {
		share = (pos->second);
	}
	if(userid.empty()||share.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate, '\0', memsize);
	snprintf(sqlstate, memsize,"update t_personal_infoshare set int_share ='%d' where vch_userid = '%s';",atoi(share.c_str()),userid.c_str());
	SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));

	try{
		query.reset();
		query.exec("set names utf8");
		if (!query.exec(sqlstate)) {
			retcode = 404;
		}
		retcode = 401;
	}catch(mysqlpp::Exception&){
		SVC_LOG((LM_ERROR, "HandleSql:update share flag error! "));
		retcode = 404;
		return false;
	}
	return true;
}

bool FDSocialTeam::FindNeigbour(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string userid,latitude,longitude;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("latitude");
	if (pos != m_UrlParams.end()) {
		latitude = pos->second;
	}
	pos = m_UrlParams.find("longitude");
	if (pos != m_UrlParams.end()) {
		longitude = pos->second;
	}
	if(userid.empty()||latitude.empty()||longitude.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	snprintf(sqlstate,memsize,QUERY_DIS_SQL,latitude.c_str(),latitude.c_str(),longitude.c_str(),userid.c_str(),longitude.c_str(),latitude.c_str(),longitude.c_str(),latitude.c_str(),latitude.c_str(),latitude.c_str(),latitude.c_str(),latitude.c_str(),longitude.c_str(),userid.c_str(),longitude.c_str(),latitude.c_str(),longitude.c_str(),latitude.c_str(),latitude.c_str(),latitude.c_str());
    SVC_LOG((LM_INFO,"SQLstate:%s\n",sqlstate));
	try{
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult CertainRes = query.store();
		if(!CertainRes.empty()){
			GenericRetString = ResToJson(CertainRes);
		};
		retcode = 401;
	}catch(mysqlpp::Exception&){
        SVC_LOG((LM_ERROR,"SQLerror:%s",conn->error()));
    	retcode = 404;
		return false;
	}
	return true;
}
//模糊查找
bool FDSocialTeam::FindCertainUser(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string userid,friendid,select;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("select");
	if (pos != m_UrlParams.end()) {
		select = pos->second;
	}
	pos = m_UrlParams.find("friendid");
	if (pos != m_UrlParams.end()) {
		query.escape_string(&friendid,(pos->second).c_str(),(pos->second).length());
		//friendid = pos->second;
	}
	if(userid.empty()||select.empty()||friendid.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	if(atoi(select.c_str())== 1){ //vague lookup
		snprintf(sqlstate, memsize,"select a.vch_userid as userid,a.vch_username as username,a.vch_gender as gender,a.vch_url as url,1 as flag from t_personal_info a where a.vch_userid in(select vch_friend_id from t_friend_info where vch_userid = '%s' and int_sheild = 1)and a.vch_userid <> '%s' and a.vch_username like binary concat('%','%s','%') union select a.vch_userid as userid,a.vch_username as username,a.vch_gender as gender,a.vch_url as url,0 as flag from t_personal_info a where a.vch_userid not in(select vch_friend_id from t_friend_info where vch_userid = '%s'and int_sheild = 1) and a.vch_userid <> '%s' and a.vch_username like binary concat('%','%s','%') order by username desc; ",userid.c_str(),userid.c_str(),friendid.c_str(),userid.c_str(),userid.c_str(),friendid.c_str());
	}else if(atoi(select.c_str())== 2){//special id lookup
		snprintf(sqlstate,memsize," select e.userid,e.record,e.rank as historyrank,e.standard,f.vch_username as username,f.vch_url as url,g.vch_level as level,datediff(curdate(),date(f.date_update)) as useddays,case when exists(select * from t_friend_info where vch_userid = '%s' and vch_friend_id = '%s'and int_sheild = 1) then 1 else 0 end as flag from(select c.userid,c.record,(@rowno:=@rowno+1) as rank,c.standard from  (select a.vch_userid as userid, sum(a.int_record) as record,sum(a.int_standard) as standard from t_step_record_share a group by a.vch_userid order by record desc)c, (select (@rowno:=0)) d)e,t_personal_info f,t_personal_score_level g where e.userid = f.vch_userid and e.userid = g.vch_userid and e.userid ='%s';",userid.c_str(),friendid.c_str(),friendid.c_str());
	}
	SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
	try{
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult CertainRes = query.store();
        if(!CertainRes.empty()){
    		GenericRetString = ResToJson(CertainRes);
        }
		retcode = 401;
	}catch(mysqlpp::Exception&){
		retcode = 404;
		return false;
	}
}
bool FDSocialTeam::AddDelFriend(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string userid,friendid,action;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("friendid");
	if (pos != m_UrlParams.end()) {
		friendid = pos->second;
	}
	pos = m_UrlParams.find("action");
	if (pos != m_UrlParams.end()) {
		action = pos->second;
	}
	if(userid.empty()||friendid.empty()||action.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	//call the procedure proc_add_friend
	snprintf(sqlstate, memsize,"call proc_add_friend(%s,%s,%d);",userid.c_str(),friendid.c_str(),atoi(action.c_str()));
	SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
	try {
		query.reset();
		query.exec("set names utf8");
		if (!query.exec(sqlstate)) {
			retcode = 404;
			return false;
		}
		if(1==atoi(action.c_str())){
			if(!HandleAddScore(query,friendid,2)){
				retcode=404;
				return false;
			}
		}
		retcode = 401;
	} catch (mysqlpp::Exception&) {
		SVC_LOG((LM_ERROR, "HandleSql:update share flag error:%s! ",conn->error()));
		retcode = 404;
		return false;
	}
	return true;
}

bool FDSocialTeam::InitSocialFriendship(mysqlpp::Connection *conn,char * sqlstate,Json::Value& FriendShipRank,Json::Value& GenericRetString,Json::Value& CommentString){
	string userid,select,specifyid;
	StringMap::iterator pos;
	int page = 0,record_begin;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("page");
	if (pos != m_UrlParams.end()) {
		page = atoi(pos->second.c_str());
		record_begin = (page-1)*5;
	}
	pos = m_UrlParams.find("select");
	if (pos != m_UrlParams.end()) {
		select = pos->second.c_str();
	}
	if(userid.empty()|| page <= 0||select.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	if(atoi(select.c_str()) == 1){
	    pos = m_UrlParams.find("specifyid");
    	if (pos != m_UrlParams.end()) {
    		specifyid = pos->second.c_str();
	    }

		if(specifyid.empty()){
			retcode = 402;
			return false;
		}
		//personal info
		memset(sqlstate,'\0',memsize);
		snprintf(sqlstate,memsize,"select a.vch_userid as userid,a.vch_username as username,a.vch_url as url,b.int_id as sharekey,b.date_record as daterecord,b.int_record as record from t_personal_info a,t_step_record_share b where a.vch_userid = '%s' and a.vch_userid = b.vch_userid order by b.date_upload desc limit %d,5;",specifyid.c_str(),record_begin);
		try{
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult ShareContent = query.store();
			if(!ShareContent.empty()){
				GenericRetString = ResToJson(ShareContent);
			}
		}catch(mysqlpp::Exception&){

		}
	}else if(atoi(select.c_str()) == 2){
		//friend rank
		memset(sqlstate,'\0',memsize);
		snprintf(sqlstate,memsize,"select c.vch_userid as userid, c.int_record as record,c.vch_username as username,c.vch_url as url,(@rownum:=@rownum+1)as rank  from  (select b.vch_userid,a.int_record,b.vch_username,b.vch_url from t_step_record_share a,t_personal_info b where date(a.date_record) = curdate() and  a.vch_userid = b.vch_userid and a.vch_userid in(select vch_friend_id from t_friend_info where vch_userid = '%s' and int_sheild = 1) order by int_record desc limit 3) c, (select (@rownum :=0) ) b union select  c.vch_userid as userid, c.int_record as record,c.vch_username as username,c.vch_url as url,c.rank from   (select a.vch_userid,a.int_record,b.vch_username,b.vch_url,(@rowno:=@rowno+1)as rank  from t_step_record_share a ,t_personal_info b,(select (@rowno:=0)) d  where date(a.date_record) = curdate() and a.vch_userid = b.vch_userid and a.vch_userid in(select vch_friend_id from t_friend_info where vch_userid = '%s'and int_sheild = 1)  order by a.int_record desc)c  where c.vch_userid ='%s';",userid.c_str());
		try{
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult FriendRank = query.store();
			if(!FriendRank.empty()){
				FriendShipRank = ResToJson(FriendRank);
			}
		}catch(mysqlpp::Exception&){
			retcode = 404;
			SVC_LOG((LM_ERROR, "SQL_ERROR:%s!",conn->error()));
			return false;
		}
		//comment and sport share
		snprintf(sqlstate,memsize,"select a.vch_userid as userid,a.vch_username as username,a.vch_url as url,b.int_id as sharekey,b.date_record as daterecord,b.int_record as record from t_personal_info a,t_step_record_share b where (a.vch_userid = '%s' or a.vch_userid in(select vch_friend_id from t_friend_info  where vch_userid =  '%s' and int_sheild = 1)) and a.vch_userid = b.vch_userid order by b.date_upload desc limit %d,5;",userid.c_str(),userid.c_str(),record_begin);
		SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
		try{
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult ShareContent = query.store();
			if(!ShareContent.empty()){
				GenericRetString = ResToJson(ShareContent);
			}
			string key = ResKey(ShareContent,"sharekey");
			if(key.empty()){
				return false;
			}else{
				memset(sqlstate,'\0',memsize);
				snprintf(sqlstate,memsize,"select a.int_id as id, a.int_shareid as sharekey,a.vch_userid as shareuserid,b.vch_username as username,b.vch_url as url,a.int_flag as flag,a.vch_comment as comment,a.date_comment as datecomment,a.vch_cmtid as cmtid,a.int_comefrom as comefrom,a.vch_commentid as replayid from t_share_comment a,t_personal_info b where a.int_shareid in %s and a.int_shelled = 0 and a.vch_cmtid = b.vch_userid",key.c_str());
				SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
				query.reset();
				query.exec("set names utf8");
				query = conn->query(sqlstate);
				mysqlpp::StoreQueryResult ShareComment = query.store();
				if(!ShareComment.empty()){
					CommentString = ResToJson(ShareComment);
				}
			}
		}catch(mysqlpp::Exception&){
			retcode = 404;
			SVC_LOG((LM_ERROR, "SQL_ERROR:%s!",conn->error()));
			return false;
		}
		return true;
	}else{
		retcode = 402;
		return false;
	}
}
bool FDSocialTeam::AddComment(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString,Json::Value& CommentString){
	string recordid,shareuserid,commentid,symbol,comment,comefrom,replayid,datecomment;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("recordid");
	if (pos != m_UrlParams.end()) {
		recordid = pos->second;
	}
	pos = m_UrlParams.find("shareuserid");
	if (pos != m_UrlParams.end()) {
		shareuserid = pos->second;
	}
	pos = m_UrlParams.find("commentid");
	if (pos != m_UrlParams.end()) {
		commentid = pos->second;
	}
	pos = m_UrlParams.find("symbol");
	if (pos != m_UrlParams.end()) {
		symbol = pos->second;
	}
	pos = m_UrlParams.find("comment");
	if (pos != m_UrlParams.end()) {
		comment = pos->second;
	}
	pos = m_UrlParams.find("comefrom");
	if (pos != m_UrlParams.end()) {
		comefrom = pos->second;
	}
	pos = m_UrlParams.find("replayid");
	if (pos != m_UrlParams.end()) {
		replayid = pos->second;
	}
	if(recordid.empty()||shareuserid.empty()||commentid.empty()||
			symbol.empty()||comefrom.empty()||replayid.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	snprintf(sqlstate,memsize,"insert into t_share_comment_temp(int_shareid,vch_userid,int_flag,vch_comment,vch_cmtid,int_comefrom,vch_commentid)values('%s','%s','%s','%s','%s','%s','%s');",recordid.c_str(),shareuserid.c_str(),symbol.c_str(),comment.c_str(),commentid.c_str(),comefrom.c_str(),replayid.c_str());
	SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
	try {
		query.reset();
		query.exec("set names utf8");
		if (!query.exec(sqlstate)) {
			retcode = 404;
		}
		memset(sqlstate,'\0',memsize);
		if(atoi(symbol.c_str()) == 1){
			snprintf(sqlstate,memsize,"select a.int_id as id, a.int_shareid as sharekey,a.vch_userid as shareuserid,b.vch_username as username,b.vch_url as url,a.int_flag as flag,a.vch_comment as comment,a.date_comment as datecomment,a.vch_cmtid as cmtid,a.int_comefrom as comefrom,a.vch_commentid as replayid from t_share_comment a,t_personal_info b where a.vch_userid = '%s' and a.int_shelled = 0 and a.int_flag = 1 and a.vch_cmtid = b.vch_userid and a.vch_cmtid = '%s' and int_shareid = %d;",shareuserid.c_str(),commentid.c_str(),atoi(recordid.c_str()));
		}else if(atoi(symbol.c_str()) == 0){
			snprintf(sqlstate,memsize,"select a.int_id as id, a.int_shareid as sharekey,a.vch_userid as shareuserid,b.vch_username as username,b.vch_url as url,a.int_flag as flag,a.vch_comment as comment,a.date_comment as datecomment,a.vch_cmtid as cmtid,a.int_comefrom as comefrom,a.vch_commentid as replayid from t_share_comment a,t_personal_info b where a.vch_userid ='%s' and a.int_shelled = 0 and a.int_flag = 0 and a.vch_cmtid = b.vch_userid and a.vch_cmtid = '%s' and ifnull(a.vch_comment,'aa') = '%s' and int_shareid = %d;",shareuserid.c_str(),commentid.c_str(),comment.c_str(),atoi(recordid.c_str()));
		}else{
			retcode = 402;
			return false;
		}
		SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult ShareComment = query.store();
		if(!ShareComment.empty()){
			CommentString = ResToJson(ShareComment);
		}
		retcode = 401;
	} catch (mysqlpp::Exception&) {
		SVC_LOG((LM_ERROR, "HandleSql:update share flag error:%s! ",conn->error()));
		retcode = 404;
		return false;
	}
	return true;
}

bool FDSocialTeam::DeleteComment(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string userid,recordid,sets;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("recordid");
	if (pos != m_UrlParams.end()) {
		recordid = pos->second;
	}
	if(userid.empty()||recordid.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	snprintf(sqlstate,memsize,"select * from t_share_comment where int_id = %d and vch_cmtid = '%s';",atoi(recordid.c_str()),userid.c_str());
	SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
	try{
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult share_delete = query.store();
		if(share_delete.empty()){
			retcode = 406;
			return false;
		}
		memset(sqlstate,'\0',memsize);
		snprintf(sqlstate,memsize,"select getchild(%d) as sets",atoi(recordid.c_str()));
		SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));

		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult result_set = query.store();
		if(result_set.empty()){
			retcode = 402;
			return false;
		}
		sets.append("(");
		sets.append(result_set[0]["sets"].c_str());
		sets.append(")");
		SVC_LOG((LM_INFO, "SETS:%s! ",sets.c_str()));
		memset(sqlstate,'\0',memsize);
		snprintf(sqlstate,memsize,"update t_share_comment set int_shelled = 1 where int_id in %s;",sets.c_str());
		query.exec("set names utf8");
		if(!query.exec(sqlstate)){
			retcode = 404;
			SVC_LOG((LM_ERROR, "HandleSql:update share flag error:%s! ",conn->error()));
			return false;
		}
	}catch(mysqlpp::Exception&){
		retcode = 404;
		return false;
	}
	return true;
}

bool FDSocialTeam::SpecialComment(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString,Json::Value& CommentString){
	string userid,sharekey,specifyid;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("sharekey");
	if (pos != m_UrlParams.end()) {
		sharekey = pos->second;
	}
	pos = m_UrlParams.find("specifyid");
	if (pos != m_UrlParams.end()) {
		specifyid = pos->second;
	}

	if(userid.empty()||sharekey.empty()||specifyid.empty()){
		retcode = 402;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	snprintf(sqlstate,memsize,"select a.vch_userid as userid,a.vch_username as username,a.vch_url as url,b.int_id as sharekey,b.date_record as daterecord,b.int_record as record from t_personal_info a,t_step_record_share b where a.vch_userid = '%s' and a.vch_userid = b.vch_userid and b.int_id = '%s';",specifyid.c_str(),sharekey.c_str());

	try{
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult share_comment = query.store();
		if(share_comment.empty()){
			retcode = 402;
			return false;
		}
		GenericRetString = ResToJson(share_comment);
		memset(sqlstate,'\0',memsize);
		snprintf(sqlstate,memsize,"select a.int_id as id, a.int_shareid as sharekey,a.vch_userid as shareuserid,b.vch_username as username,b.vch_url as url,a.int_flag as flag,a.vch_comment as comment,a.date_comment as datecomment,a.vch_cmtid as cmtid,a.int_comefrom as comefrom,a.vch_commentid as replayid from t_share_comment a,t_personal_info b where a.int_shareid = %d and a.int_shelled = 0 and a.vch_cmtid = b.vch_userid;",atoi(sharekey.c_str()));
		SVC_LOG((LM_INFO, "HandleSql:sqldate:%s!",sqlstate));
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult FriendComment = query.store();
		if(!FriendComment.empty()){
			CommentString =  ResToJson(FriendComment);
		}
	}catch(mysqlpp::Exception&){
		retcode = 404;
		SVC_LOG((LM_ERROR, "HandleSql:return the special sharekey error:%s! ",conn->error()));
		return false;
	}
	return true;
}

void FDSocialTeam::Response(Json::Value GenericRetString,Json::Value FriendShipRank,Json::Value ShareContentString,Json::Value CommentString){
	ostringstream oss;
	switch(retcode){
	case 401:
		{info = "operate success.";break;}
	case 402:
		{info = "parameter error.";break;}
	case 403:
		{info = "the mysqlpp::Exception&mysqlpp::Exception&";break;}
	case 404:
		{info = "server error!";break;}
	case 405:
		{info = "protocol method error.";break;}
	case 406:
		{info = "the comment is not the user added!";break;}
	default:
		{info = "";break;}
	}

	GenericRetString["retcode"] = retcode;
	GenericRetString["info"] = info;
	GenericRetString["friendrank"] = FriendShipRank;
	GenericRetString["dataset"] = ShareContentString;
	GenericRetString["comment"] = CommentString;
	string StringReturn = GenericRetString.toStyledString();
	oss << "Content-type:application/json" << "\r\n\r\n"
		<< StringReturn
		<< "\r\n";
	string rsp = oss.str();
	oss.str("");
	rsp.insert(0, oss.str());
	SVC_LOG((LM_INFO,"JSON:%s",rsp.c_str()));
	FCGX_PutStr(rsp.c_str(), rsp.size(), m_Request.out);
	FCGX_FFlush(m_Request.out);
	FCGX_Finish_r(&m_Request);
}

bool FDSocialTeam::StepRank(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string select,userid;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	memset(sqlstate,'\0',memsize);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
    
	pos = m_UrlParams.find("select");
	if (pos != m_UrlParams.end()) {
		select = pos->second;
	}
	if(select.empty()||userid.empty()){
		retcode = 402;
		return false;
	}
	if(atoi(select.c_str())==1){//current day friend rank
		snprintf(sqlstate,memsize,"select a.vch_userid as userid, a.int_record as record,b.vch_username as username,b.vch_url as url from t_step_record_share a,t_personal_info b where a.date_record = curdate() and a.vch_userid = b.vch_userid and (a.vch_userid in(select c.vch_friend_id from t_friend_info c where c.vch_userid = '%s'and c.int_sheild = 1 ) or a.vch_userid = '%s') order by a.int_record desc limit 10;",userid.c_str(),userid.c_str());
	}else if(atoi(select.c_str()) == 2){//serven day ---->huawei need
		snprintf(sqlstate,memsize,"select f.vch_userid as userid ,f.vch_url as url,f.vch_username as username,e.record,e.rank  from(select c.userid,c.record,(@rowno:=@rowno+1) as rank from  (select a.vch_userid as userid, sum(a.int_record) as record  from t_step_record_share a where date(a.date_record) between date_add(curdate(),interval - 6 day) and curdate() group by a.vch_userid order by record desc)c,  (select (@rowno:=0)) d)e,t_personal_info f where e.userid = f.vch_userid limit 100  union select f.vch_userid as userid ,f.vch_url as url,f.vch_username as username,e.record,e.rank  from(select c.userid,c.record,(@rowno:=@rowno+1) as rank from  (select a.vch_userid as userid, sum(a.int_record) as record  from t_step_record_share a where date(a.date_record) between date_add(curdate(),interval - 6 day) and curdate() group by a.vch_userid order by record desc)c,  (select (@rowno:=0)) d)e,t_personal_info f where e.userid = f.vch_userid and f.vch_userid = '%s';",userid.c_str());
	}else{
		retcode = 402;
		return false;
	}
	try{
        SVC_LOG((LM_INFO,"SQLS:%s",sqlstate));
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult rank_set = query.store();
		if(!rank_set.empty()){
			GenericRetString =  ResToJson(rank_set);
		}
		retcode = 401;
	}catch(mysqlpp::Exception&){
		retcode = 404;
		return false;
	}
}
bool FDSocialTeam::HandleAddScore(mysqlpp::Query &query,std::string& userid,int add_type)
{
	char sqlstate[256]="";
	snprintf(sqlstate,memsize,"call proc_update_score('%s',%d);", userid.c_str(),add_type);
	SVC_LOG((LM_INFO,"SQLSTATE:%s",sqlstate));
	query.reset();
	query.exec("set names utf8");
	if(!query.exec(sqlstate))//catch exception outside
		return false;
	return true;
}

bool FDSocialTeam::RecordShare(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString){
	string where,userid;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	memset(sqlstate,'\0',memsize);
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}

	pos = m_UrlParams.find("where");
	if (pos != m_UrlParams.end()) {
		where = pos->second;
	}
	if(where.empty()||userid.empty()){
		retcode = 402;
		return false;
	}
	snprintf(sqlstate,memsize,"insert into t_infoshare_out_detail(vch_userid,vch_sharewhich)values('%s','%s');",userid.c_str(),where.c_str());
	try{
		SVC_LOG((LM_INFO,"SQLS:%s",sqlstate));
		query.exec("set names utf8");
		query.exec(sqlstate);
		retcode = 401;
	}catch(mysqlpp::Exception&){
		retcode = 404;
		return false;
	}
	return true;
}

