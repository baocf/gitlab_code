
#ifndef PRODUCT_COMMENT_H
#define PRODUCT_COMMENT_H

#include <string>
#include <map>
#include <pthread.h>
#include <mysql++.h>
#include "../public/ThreadBase.h"
#include "fcgi_stdio.h"
#include "fcgiapp.h"
#include "fcgio.h"
#include "../json/json.h"
using namespace std;
using namespace mysqlpp;


class FDShowComment : public CThreadBase {
    public:
        void run();
        int initialize();
        FDShowComment();
    private:
        std::string getEnv(const std::string& param);
        std::string getVal(const std::string& param);
        void parseQueryString(const std::string& qs);
        bool HandleSql(Json::Value& FriendShipRank,Json::Value& GenericRetString,Json::Value& CommentString);//composite the sql-statement,and excute it
        bool AddComment(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString,Json::Value& CommentString);//add comment
        bool DeleteComment(mysqlpp::Connection *conn,char * sqlstate,Json::Value& GenericRetString);//delete comment
        void Response(Json::Value GenericRetString,Json::Value FriendShipRank,Json::Value ShareContentString,Json::Value CommentString);//response to client
    private:
        std::map<std::string, std::string> m_UrlParams;
        FCGX_Request m_Request;
		int retcode;	//initialize
		string info;		//information of return
    private:
        static pthread_mutex_t m_Mutex;
};

#endif /* PRODUCT_COMMENT_H */
