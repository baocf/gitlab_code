/*
 * protocol.cpp
 *
 *  Created on: 2014-2-26
 *      Author: tanziwen
 */

#include<ctype.h>
#include "protocol.h"

using namespace std;

string& replace_all_distinct(string& str, const string& old_value,const string& new_value) {
	for (string::size_type pos(0); pos != string::npos; pos += new_value.length()) {
		if ((pos = str.find(old_value, pos)) != string::npos)
			str.replace(pos, old_value.length(), new_value);
		else
			break;
	}
	return str;
}

u_char toHex(const u_char &x) {
	return x > 9 ? x - 10 + 'A' : x + '0';
}

u_char fromHex(const u_char &x) {
	return isdigit(x) ? x - '0' : x - 'A' + 10;
}

string URLEncode(const string &sIn) {
	string sOut;
	for (size_t ix = 0; ix < sIn.size(); ix++) {
		u_char buf[4];
		memset(buf, 0, 4);
		if (isalnum((u_char) sIn[ix])) {
			buf[0] = sIn[ix];
		} else {
			buf[0] = '%';
			buf[1] = toHex((u_char) sIn[ix] >> 4);
			buf[2] = toHex((u_char) sIn[ix] % 16);
		}
		sOut += (char *) buf;
	}
	return sOut;
}

string URLDecode(const string &sIn) {
	string sOut;
	for (size_t ix = 0; ix < sIn.size(); ix++) {
		u_char ch = 0;
		if (sIn[ix] == '%') {
			ch = (fromHex(sIn[ix + 1]) << 4);
			ch |= fromHex(sIn[ix + 2]);
			ix += 2;
		} else if (sIn[ix] == '+') {
			ch = ' ';
		} else {
			ch = sIn[ix];
		}
		sOut += (char) ch;
	}
	return sOut;
}

void ChangeTolower(const char*src){
	int lenth = strlen(src);
	if(lenth <= 0||src ==NULL){
		return;
	}
	char * temp = (char*)src;
	for(int i = 0;i<lenth;i++){
		if(isupper(temp[i])){
			temp[i] = tolower(temp[i]);
		}
	}
}
