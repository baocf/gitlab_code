/*
 * 	ThreadBase.cpp
 *  Created on: 2013-8-11
 *      Author: tanker
 *      Description  : 
 */
#include "ThreadBase.h"
#include <sys/select.h>
using namespace std;

CThreadBase::CThreadBase(): m_ThreadId(0), m_Shutdown(false) {
   pthread_attr_init(&m_Attr);	
}

CThreadBase::~CThreadBase() {}

void* CThreadBase::threadFunction(void *pHandle) {
    CThreadBase* pUser=(CThreadBase*)pHandle;
    pUser->run();
	return NULL;
}

int CThreadBase::initialize() {
    return 0;
}

int CThreadBase::start() {

	if(-1 == pthread_create(&m_ThreadId, &m_Attr,threadFunction, this)) {
       pthread_attr_destroy(&m_Attr);	
       return -1;
    }
    pthread_attr_destroy(&m_Attr);	 
    return 0;
}

void CThreadBase::join(void) {
    if(0 != m_ThreadId)
        pthread_join(m_ThreadId, NULL);
   
}

void CThreadBase::shutdown(void) {
    m_Shutdown = true;
    join();
}

void CThreadBase::wait(int sec) {
    struct timeval tv;
    tv.tv_sec += sec;
    select(0, NULL, NULL, NULL, &tv);
}

void CThreadBase::uwait(int sec) {
    struct timeval tv;
    tv.tv_usec += sec;
    select(0, NULL, NULL, NULL, &tv);
}
