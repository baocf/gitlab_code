/*
 * datachange.cpp
 *  Created on: 20142-26
 *      Author: tanziwen
 */

#include "datachange.h"
using namespace std;

//table to json
Json::Value ResToJson(mysqlpp::StoreQueryResult res) {
	Json::Value jsondata;
	int Row = res.num_rows();
	for (int i = 0; i < Row; ++i) {
		Json::Value json_temp;
		for (int j = 0; j < int(res.num_fields()); ++j) {
			string strKey = res.field_name(int(j));
			string strValue = res[i][j].c_str();
		/*  if (strValue.empty()) {
				strValue = "";
			}*/
			json_temp[strKey] = strValue;
		}
		jsondata.append(json_temp);
	}
	if (jsondata.empty()) {
        return "";
    }

	return jsondata;
}
//step count
string JsonToDate_step(const string &user_id,const string jsondate) {

	if (NULL == jsondate.c_str()) {
		return " ";
	}
	string str_userid = user_id;
	string strValue;
	Json::Reader reader;
	Json::Value value;
	strValue.append("{\"arrayobj\":");
	strValue += jsondate;
	strValue.append("}");

	SVC_LOG((LM_INFO, "JsonToDate:jsonparm:%s! ",strValue.c_str()));

	StringMap var_json;
	string linkdbset;
	if (reader.parse(strValue, value, false)) {
		SVC_LOG((LM_INFO, "JsonToDate:parse the jsondate"));
		Json::Value::Members member;
        const Json::Value arrayObj = value["arrayobj"];
		for (Json::Value::iterator itr = arrayObj.begin();
				itr != arrayObj.end(); itr++) {
			var_json.clear();
			member = (*itr).getMemberNames();
				for (Json::Value::Members::iterator iter = member.begin();
					iter != member.end(); iter++) {
				string str_temp = (*itr)[(*iter)].asString();
				var_json.insert(make_pair(*iter, (*itr)[(*iter)].asString()));
			}
			StringMap::iterator str_pos;
			linkdbset.append("(");
			linkdbset.append("\"");
			linkdbset.append(str_userid);
			linkdbset.append("\",");

			str_pos = var_json.find("step");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("calories");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("distance");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}

			str_pos = var_json.find("datetime");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}

			linkdbset.replace(linkdbset.length() - 1, 1, ")");
			linkdbset += ",";
		}

		linkdbset.replace(linkdbset.length() - 1, 1, "\0");
		SVC_LOG((LM_INFO, "JsonToDate:retjson:%s! ",linkdbset.c_str()));
		return linkdbset;
	}
	return "";
}


//sleep quality
string JsonToDate_sleep(const string & user_id,const string &jsondate) {
	if (NULL == jsondate.c_str()) {
			return " ";
		}
	string userid = user_id;
	string strValue;
	Json::Reader reader;
	Json::Value value;
	strValue.append("{\"arrayobj\":");
	strValue += jsondate;
	strValue.append("}");

	SVC_LOG((LM_INFO, "JsonToDate:jsonparm:%s! ",strValue.c_str()));

	StringMap var_json;
	string linkdbset;
	if (reader.parse(strValue, value, false)) {
		Json::Value::Members member;
		const Json::Value arrayObj = value["arrayobj"];
		for (Json::Value::iterator itr = arrayObj.begin();itr != arrayObj.end(); itr++) {
			var_json.clear();
			member = (*itr).getMemberNames();
			for (Json::Value::Members::iterator iter = member.begin();	iter != member.end(); iter++) {
				string str_temp = (*itr)[(*iter)].asString();
				var_json.insert(make_pair(*iter, (*itr)[(*iter)].asString()));
			}
			StringMap::iterator str_pos;
			linkdbset.append("(");
			linkdbset += "\"" + userid + "\"" + ',';

			str_pos = var_json.find("timebegin");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("timeend");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("count");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("occurtime");
			{
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			linkdbset.replace(linkdbset.length() - 1, 1, ")");
			linkdbset += ",";
		}

		linkdbset.replace(linkdbset.length() - 1, 1, "\0");
		SVC_LOG((LM_INFO, "JsonToDate:retjson:%s! ",linkdbset.c_str()));
		return linkdbset;
	}
	return " ";
}

//position trace
string JsonToDate_detail(const string &user_id,const string &jsondate) {

	if (NULL == jsondate.c_str()) {
		return " ";
	}
	string userid = user_id;
	string strValue;
	Json::Reader reader;
	Json::Value value;
	strValue.append("{\"arrayobj\":");
	strValue += jsondate;
	strValue.append("}");

	SVC_LOG((LM_INFO, "JsonToDate:jsonparm:%s! ",strValue.c_str()));

	StringMap var_json;
	string linkdbset;
	if (reader.parse(strValue, value, false)) {
		Json::Value::Members member;
		const Json::Value arrayObj = value["arrayobj"];
		for (Json::Value::iterator itr = arrayObj.begin();itr != arrayObj.end(); itr++) {
			var_json.clear();
			member = (*itr).getMemberNames();
			for (Json::Value::Members::iterator iter = member.begin();
					iter != member.end(); iter++) {
				string str_temp = (*itr)[(*iter)].asString();
				var_json.insert(make_pair(*iter, (*itr)[(*iter)].asString()));
			}
			StringMap::iterator str_pos;
			linkdbset.append("(");
			//here format the table order
			linkdbset += "\"" + userid + "\"" + ',';

			str_pos = var_json.find("longitude");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("latitude");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("flag");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("addTime");
			{
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("address");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			linkdbset.replace(linkdbset.length() - 1, 1, ")");
			linkdbset += ",";
		}

		linkdbset.replace(linkdbset.length() - 1, 1, "\0");
		SVC_LOG((LM_INFO, "JsonToDate:retjson:%s! ",linkdbset.c_str()));
		return linkdbset;
	}
	return " ";
}
//json format ---> datebase format order(vch_userid,int_step,int_sleep,time_setting)
string JsonToSetting(const string &user_id,const string &jsondate) {

	if (jsondate.empty()) {
		return " ";
	}
	string userid = user_id;
	string strValue;
	Json::Reader reader;
	Json::Value value;
	strValue.append("{\"arrayobj\":");
	strValue += jsondate;
	strValue.append("}");

	SVC_LOG((LM_INFO, "JsonToSetting:jsonparm:%s! ",strValue.c_str()));

	StringMap var_json;
	string linkdbset;
	if (reader.parse(strValue, value, false)) {
		Json::Value::Members member;
		const Json::Value arrayObj = value["arrayobj"];
		for (Json::Value::iterator itr = arrayObj.begin();itr != arrayObj.end(); itr++) {
			var_json.clear();
			member = (*itr).getMemberNames();
			for (Json::Value::Members::iterator iter = member.begin();
					iter != member.end(); iter++) {
				string str_temp = (*itr)[(*iter)].asString();
				var_json.insert(make_pair(*iter, (*itr)[(*iter)].asString()));
			}
			StringMap::iterator str_pos;
			linkdbset.append("(");
			//here format the table order
			linkdbset += "\"" + userid + "\"" + ',';

			str_pos = var_json.find("step");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("sleep");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			str_pos = var_json.find("timesetting");
			if (str_pos != var_json.end()) {
				linkdbset += "\"" + str_pos->second + "\"" + ',';
			}
			linkdbset.replace(linkdbset.length() - 1, 1, ")");
			linkdbset += ",";
		}

		linkdbset.replace(linkdbset.length() - 1, 1, "\0");
		SVC_LOG((LM_INFO, "JsonToDate:retjson:%s! ",linkdbset.c_str()));
		return linkdbset;
	}
	return " ";
}

//social team
string ResKey(mysqlpp::StoreQueryResult res,const string key){
	if(key.empty()||res.empty()){
		return "";
	}
	string ret_key;
	ret_key.append("(");
	int Row = res.num_rows();
	for (int i = 0; i < Row; ++i) {
		for(int j = 0; j < int(res.num_fields()); ++j){
			if(string(res.field_name(j)) == key){
				ret_key.append(res[i][j]);
				ret_key.append(",");
			}
		}
	}
	ret_key.replace(ret_key.length()-1,1,")");
	return ret_key;
}



