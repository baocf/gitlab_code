/*
 *  DBConnectPool.h
 *  Created on: 2014-2-26
 *  Author: tanziwen
 */

#ifndef DBCONNECTPOOL_H_
#define DBCONNECTPOOL_H_

#include<mysql++.h>
#include<pthread.h>
#include<list>
#include<string>

using namespace std;
using namespace mysqlpp;


class ConnPool {

private:
	Connection*CreateConnection(); //create a connection
	void InitConnection(int iInitialSize); //initialize pool
	bool Isuseable(Connection*conn);
	void DestoryConnection(Connection *conn); //destroy connections
	ConnPool(){}; //construct function
public:
	~ConnPool(){};
	bool InitPool(string _dbname,string _hostname,string _username,string _password,int _port,int _size );
	Connection*GetConnection();
	void ReleaseConnection(mysqlpp::Connection *conn); //release a used connecto to pool
	static ConnPool *GetInstance(); //get the only instance of pool
	void DestoryConnPool(); //destroy the connection pool
private:
	int curSize; //number of created connections 
	int maxSize; //the maxsize of pool
	string username;
	string password;
	string hostname;
	string dbname;
	int port;
	list<Connection*> connList; //list of pool
	pthread_mutex_t lock	; //thread lock
	static ConnPool *connPool;
	DBDriver*driver;
};

#endif /* CONNECTPOOL_H_ */
