/*
 * 	md5.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 *      Description  : md5
 */
#ifndef MD5_H
#define MD5_H

#include "typedef.h"
typedef struct
{
	u_int count[2];
	u_int state[4];
	u_char buffer[64];
}MD5_S;


#define F(x,y,z) ((x & y) | (~x & z))
#define G(x,y,z) ((x & z) | (y & ~z))
#define H(x,y,z) (x^y^z)
#define I(x,y,z) (y ^ (x | ~z))
#define ROTATE_LEFT(x,n) ((x << n) | (x >> (32-n)))
#define FF(a,b,c,d,x,s,ac) \
{ \
	a += F(b,c,d) + x + ac; \
	a = ROTATE_LEFT(a,s); \
	a += b; \
}
#define GG(a,b,c,d,x,s,ac) \
{ \
	a += G(b,c,d) + x + ac; \
	a = ROTATE_LEFT(a,s); \
	a += b; \
}
#define HH(a,b,c,d,x,s,ac) \
{ \
	a += H(b,c,d) + x + ac; \
	a = ROTATE_LEFT(a,s); \
	a += b; \
}
#define II(a,b,c,d,x,s,ac) \
{ \
	a += I(b,c,d) + x + ac; \
	a = ROTATE_LEFT(a,s); \
	a += b; \
}
void MD5Init(MD5_S *context);
void MD5Update(MD5_S *context,u_char *input,u_int inputlen);
void MD5Final(MD5_S *context,u_char digest[16]);
void MD5Transform(u_int state[4],u_char block[64]);
void MD5Encode(u_char *output,u_int *input,u_int len);
void MD5Decode(u_int *output,u_char *input,u_int len);

#endif
