/*
 * 	synch.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 *      Description  : deal the multi-thread synchronize problem
 */

#ifndef _SYNCH_H_
#define _SYNCH_H_
#include <pthread.h>
using namespace std;

//mutex
class CThread_Mutex {
public:
	CThread_Mutex() {
		pthread_mutex_init(&this->m_lock, NULL);
	}

	~CThread_Mutex() {
		pthread_mutex_destroy(&this->m_lock);
	}

	int acquire() {
		return pthread_mutex_lock(&this->m_lock);
	}

	int release() {
		return pthread_mutex_unlock(&this->m_lock);
	}

	pthread_mutex_t &lock(void) {
		return this->m_lock;
	}

private:
	CThread_Mutex(const CThread_Mutex &);
	CThread_Mutex& operator=(const CThread_Mutex &);

private:
	pthread_mutex_t m_lock;
};

//conditional variable
class CThread_Condition {
public:
	CThread_Condition(CThread_Mutex &m) :
		m_mutex(m) {
		pthread_cond_init(&this->m_cond, NULL);
	}

	~CThread_Condition(void) {
		pthread_cond_destroy(&this->m_cond);
	}

	int wait(void) {
		return pthread_cond_wait(&this->m_cond, &(this->m_mutex.lock()));
	}

	int signal(void) {
		return pthread_cond_signal(&this->m_cond);
	}

private:
	CThread_Condition(const CThread_Condition &);
	CThread_Condition &operator=(const CThread_Condition &);

private:
	pthread_cond_t m_cond;
	CThread_Mutex &m_mutex;
};

//lock-guard
template<class LOCK> class CMutex_Guard {
public:
	CMutex_Guard(LOCK &m);
	~CMutex_Guard(void);

private:
	CMutex_Guard(const CMutex_Guard<LOCK> &);
	CMutex_Guard &operator=(const CMutex_Guard<LOCK> &);

private:
	LOCK *m_pmutex;
};

template<class LOCK> inline CMutex_Guard<LOCK>::CMutex_Guard(LOCK &m) :
	m_pmutex(&m) {
	this->m_pmutex->acquire();
}

template<class LOCK> inline CMutex_Guard<LOCK>::~CMutex_Guard(void) {
	this->m_pmutex->release();
}

class Atomic_Op {
public:
	Atomic_Op() {
		pthread_mutex_init(&this->m_lock, NULL);
		i = 0;
	}

	Atomic_Op(int c) {
		pthread_mutex_init(&this->m_lock, NULL);
		i = c;
	}

	~Atomic_Op() {
		pthread_mutex_destroy(&this->m_lock);
	}

	int operator++(void) {
		pthread_mutex_lock(&this->m_lock);
		++this->i;
		pthread_mutex_unlock(&this->m_lock);
		return this->i;
	}

	int operator--(void) {
		pthread_mutex_lock(&this->m_lock);
		--this->i;
		pthread_mutex_unlock(&this->m_lock);
		return this->i;
	}

	int value(void) {
		return this->i;
	}

private:
	pthread_mutex_t m_lock;
	int i;
};

//thread-count guard
class CCount_Guard {
public:
	CCount_Guard(Atomic_Op &op) :
		m_op(op) {
		++m_op;
	}

	~CCount_Guard(void) {
		--m_op;
	}
private:
	Atomic_Op &m_op;
};

#endif
