/*
 * protocol.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include<string.h>
#include "typedef.h"

using namespace std;



//replace specialize characte
string& replace_all_distinct(string& str, const string& old_value,const string& new_value);
//url encode decode
u_char toHex(const u_char &x);
u_char fromHex(const u_char &x);
string URLEncode(const string &sIn);
string URLDecode(const string &sIn);

//upper to lower
void ChangeTolower(const char*src);

#endif /* PROTOCOL_H_ */
