/*
 * 	ThreadBase.h
 *  Created on: 2013-8-11
 *      Author: tanker
 *      Description  : 
 */
#ifndef THREAD_BASE_H
#define THREAD_BASE_H

#include <pthread.h>
using namespace std;

class CThreadBase {
public: 
    CThreadBase(); 
    virtual ~CThreadBase();     
protected: 
    static void* threadFunction(void *pHandle);
    bool  m_Shutdown;    
public:
    virtual int  initialize();
    int start(); //Start to execute the thread 
    virtual void run(void)=0;
    void shutdown(void); //Terminate the thread 
    pthread_t getThreadId(void){return (m_ThreadId);} 
    void join(void); 
public:
    static void wait(int second);
    static void uwait(int usecond);    
private: 
    pthread_t m_ThreadId; 
    pthread_attr_t m_Attr;
};

#endif

