/*
 * profile.cpp
 *  Created on: 2014-2-26
 *      Author: tanizwen
 *      Description  : profile functions describled
 */

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "profile.h"
#include <sys/stat.h>
using namespace std;

const char *Section::getString(const char *name, const char *def) {
	StringMap::iterator it = pairs.find(name);
	if (it == pairs.end())
		return def;
	return (*it).second.c_str(); //return the point of the second value
}

int Section::getInteger(const char *name, int def) {
	const char *val = getString(name, NULL);
	if (!val)
		return def;
	return atoi(val);
}

inline void skipWhiteSpace(const char *&p) {
	while (*p && (*p == ' ' || *p == '\t'))
		p++;
}

static bool parseSection(std::string &name, const char *line) {
	char buf[256];
	char *p = buf;
	line++;
	while (*line && *line != ']')
		*p++ = *line++;
	*p = '\0';
	if (!*line)
		return false;
	//address migrate
	name = buf;
	return true;
}

static bool parsePair(StringMap &pairs, const char *line) {
	char name[256];
	char val[256];
	char *p = name;

	//analyse name section
	while (*line && *line != '=' && !isspace(*line))
		*p++ = *line++;
	*p = '\0';
	if (!*name)				//TODO
		return false;

	skipWhiteSpace(line);
	if (*line != '=')
		return false;

	skipWhiteSpace(++line);

	//analyse value section
	p = val;
	while (*line && *line != '\r' && *line != '\n')
		*p++ = *line++;
	*p = '\0';

	if (*val) {
		char *q = strchr(val, '#');
		if (q) {
			*q-- = '\0';
			while (*q == '\t' || *q == ' ') {
				*q-- = '\0';
			}
		}
		pairs[name] = val;	//insert date
	}

	return true;
}

bool parseProfile(const char *name, ON_SECTION_PARSED onSectionParsed) {
	FILE *file = fopen(name, "r");	//TODO
	if (!file)
		return false;
	//fprintf(stdout,"open the profile ok! \n");

	Section sec;
	char line[256];

	while (fgets(line, sizeof(line), file)) {
		if (*line == '#')
			continue;

		if (*line == '[') {
			if (!sec.name.empty()) {
				onSectionParsed(sec);
				sec.name = "";
				sec.pairs.clear();
			}
			parseSection(sec.name, line);
		} else
			parsePair(sec.pairs, line);
	}

	if (!sec.name.empty())
		onSectionParsed(sec);

	fclose(file);
	return true;
}
//file create
int CreateDir(const char *sPathName) {
	char DirName[256];
	strcpy(DirName, sPathName);
	int i, len = strlen(DirName);
	if (DirName[len - 1] != '/')
		strcat(DirName, "/");

	len = strlen(DirName);

	for (i = 1; i < len; i++) {
		if (DirName[i] == '/') {
			DirName[i] = 0;
			if (access(DirName, NULL) != 0) {
				if (mkdir(DirName, 0755) == -1) {
					perror("mkdir   error");
					return -1;
				}
			}
			DirName[i] = '/';
		}
	}

	return 0;
}

//---------------------------------------------

