/*
 * profile.h
 *  Created on: 2014-2-26
 *      Author: tanker
 *      Description  : analyse and decode the profile file section
 */
#ifndef _PROFILE_H
#define _PROFILE_H

#include <string>
#include <map>
#include "typedef.h"
using namespace std;


class Section;
typedef bool (*ON_SECTION_PARSED)(Section &sec);

class Section {
public:
	const char *getName() {
		return name.c_str();
	}
	const char *getString(const char *name, const char *def = "");
	int getInteger(const char *name, int def);
	friend bool parseProfile(const char *name, ON_SECTION_PARSED onSectionParsed);

private:
	std::string name;
	StringMap pairs;
};
//---------------------------------public function--------------------
//file create
int CreateDir(const char *sPathName);

//database info
typedef struct DB_INFO {
	string Host;
	u_int16 Port;
	string DB;
	string User;
	string Passwd;
	int LinkNum;
}Db_info;

//log info
typedef struct LOG_INFO{
	string LogPath;
	string LogName;
	int     LogLevel;
	int 	ThreadNumber;
	bool	MultiProcess;
}Log_info;

//customization,current image used it
typedef struct CUSTOM_INFO{
	string ImagePathName;
}Image_info;
//profile :all section defined here
struct OPTIONS_S {
	// log
	LOG_INFO Log_info;
	// database
	DB_INFO DbInfo;
	//customization
	CUSTOM_INFO image;
};


#endif
