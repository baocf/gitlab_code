/*
 *	typedef.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 *      Description  : this file defind the type used in program
 */

#ifndef CONST_H_
#define CONST_H_
#include <string>
#include <map>
using namespace std;


typedef unsigned char	u_char;
typedef unsigned int	u_int;
typedef unsigned short	u_int16;
typedef unsigned long	u_int32;

//typedef long long  int64_t;
//typedef unsigned long long   uint64_t;

typedef std::map<std::string,std::string>StringMap;

#endif /* CONST_H_ */
