/*
 * 	Log.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 *      Description  : log 
 */

#ifndef LOG_H_
#define LOG_H_

#include <fstream>
#include <list>
#include <pthread.h>
#include <string>
using namespace std;

#define LL_DEBUG 1
#define LL_INFO 2
#define LL_WARN 4
#define LL_ERROR 8

#define MAX_LOG_BUF_SIZE 10240

//the maxsize of log list
const unsigned int MAX_LOG_NUMBER = 20000;

#define LM_DEBUG 	LL_DEBUG,__LINE__,__FILE__
#define LM_INFO 	LL_INFO,__LINE__,__FILE__
#define LM_WARN 	LL_WARN,__LINE__,__FILE__
#define LM_ERROR 	LL_ERROR,__LINE__,__FILE__

#define SVC_LOG(x) \
do { \
    CLog *plog__ = CLog::getInstance(); \
    plog__->log x; \
}while(0)


class CLog {
public:
	//log size
	enum {
		//1000M
		MAX_SIZE = 1048576000
	};

	//log level:debug,info,warn,error
	enum {
		LOG_LEVEL_DEBUG = 1,
		LOG_LEVEL_INFO = 2,
		LOG_LEVEL_WARN = 4,
		LOG_LEVEL_ERROR = 8
	};

private:
	CLog(void);
	~CLog(void);

public:
	static CLog *getInstance(void);//single console
	bool initialize(const char *pszPath, const char *pszFName);//just called at initialize
	void finalize(void);//just called at end of the program or the log initialize failed

	//start log thread,ruturn id of thread if sucess else -1
	int open(int iNum);

	//stop log thread
	int close();

	//get log level
	int getLogLevel(void);

	//set log level
	void setLogLevel(int level);

	//set log level
	void setLogLevel(const char *pszLvl);

	//set the option of muli-thread support
	void setMulithtread (bool bl){ bMultiProcess = bl;}

	//set the maxsize of log
	void setMaxSize(int size) {
		m_iMaxSize = size;
	}

	int log(int log_priority, int nline, const char *file,const char *format_str, ...);

	std::ofstream& operator<<(const char *buf);

	static void *svc_run(void *param);

	int svc(void);

private:
	int getDate(std::string &strDate);
	int lock(int fd);
	int unlock(int fd);

private:
	CLog(const CLog &);
	CLog &operator=(const CLog &);

private:
	static CLog *m_pLog;
	std::ofstream fp;
	char *m_pszPath; //log path
	char *m_pszFName; //log name ,the first part of whole 
	std::string m_strDir; //the first part of log-name
	pthread_mutex_t m_logmutex; //log incompatiable variable
	int m_LogLevel;
	bool bFinished;
	int m_iNum; //index
	std::string m_strDate; //saved date
	std::list<pthread_t> m_lstThrId;
	pthread_mutex_t m_lstmutex; //log list incompatiable variable
	pthread_cond_t m_lstcond; //log list condition variable
	std::list<std::string> m_lstBuf;

	bool bMultiProcess;
	int m_LockFd;
	int m_iMaxSize;
};

#endif /* LOG_H_ */
