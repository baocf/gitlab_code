/*
 * DBConnectPool.cpp
 *  Created on: 2014-2-26
 *  Author: tanziwen
 */
#include<stdexcept>
#include<exception>
#include<stdio.h>
#include<pthread.h>
#include "DBConnectPool.h"

using namespace std;
using namespace mysqlpp;

ConnPool*ConnPool::connPool = NULL;
//construct function
bool ConnPool::InitPool(string _dbname, string _hostname, string _username,	string _password, int _port, int _size) {
	this->maxSize = _size;
	this->curSize = 0;
	this->dbname = _dbname;
	this->username = _username;
	this->password = _password;
	this->hostname = _hostname;
	this->port = _port;
	pthread_mutex_init(&lock,NULL);
	//create connections when initialize the pool
	this->InitConnection(_size);
	return true;
}

//get the only instance of pool,
ConnPool*ConnPool::GetInstance() {
	if (connPool == NULL) {
		connPool = new ConnPool;
	}
	return connPool;
}
//initialize the pool:create specified numbers connection
void ConnPool::InitConnection(int iInitialSize) {
	Connection*conn = NULL;
	pthread_mutex_lock(&lock);
	for (int i = 0; i < iInitialSize; i++) {
		conn = this->CreateConnection();
		if (conn) {
			connList.push_back(conn);
			++(this->curSize);
		} else {
			perror("create connection failed");
		}
	}
	pthread_mutex_unlock(&lock);
}
//create the pool and return Connection handle
Connection* ConnPool::CreateConnection() {

	Connection *conn = new Connection(this->dbname.c_str(),
			this->hostname.c_str(), this->username.c_str(),
			this->password.c_str(), this->port);
	if (conn) {
		return conn; //return the connection handle
	} else {
		return NULL;
	}

}
//get a connection from pool
Connection*ConnPool::GetConnection() {
	Connection*conn = NULL;
	pthread_mutex_lock(&lock);
	if (connList.size() > 0) //the pool have usable connection yet
	{
		//this method can inprove 
		conn = connList.front(); //get the first connection handle
		connList.pop_front(); //remove the first connection handle
		if (!conn->ping()) //if the connection is closed ,create it
		{
			delete conn;
			conn = this->CreateConnection();
		}
		//erron :create connection failed or the original connection is not usable
		if (conn == NULL) {
			--curSize;
		}
		pthread_mutex_unlock(&lock);
		return conn;
	} else {
		if (curSize < maxSize) { //not reach the maxsize
			conn = this->CreateConnection();
			if (conn) {
				++curSize;
				pthread_mutex_unlock(&lock);
				return conn;
			} else {
				pthread_mutex_unlock(&lock);
				return NULL;
			}
		} else { //already reach maxSize
			pthread_mutex_unlock(&lock);
			return NULL;
		}
	}
}
//release the used connection
void ConnPool::ReleaseConnection(mysqlpp::Connection * conn) {
	if (conn) {
		pthread_mutex_lock(&lock);
		connList.push_back(conn);
		pthread_mutex_unlock(&lock);
	}
}

//destroy the connection pool
void ConnPool::DestoryConnPool() {
	list<Connection*>::iterator icon;
	pthread_mutex_lock(&lock);
	for (icon = connList.begin(); icon != connList.end(); ++icon) {
		this->DestoryConnection(*icon); //destroy the member of connetion pool
	}
	curSize = 0;
	connList.clear(); //clear the data of list
	pthread_mutex_unlock(&lock);
}
//destroy a connection
void ConnPool::DestoryConnection(Connection* conn) {
	if (conn) {
		try {
			conn->disconnect();
		} catch (...) {
			perror("destory connect error!");
		}
		delete conn;
	}
}

