/*
 * base64.cpp
 *  Created on: 2014-2-26
 *      Author: tanziwen
 */
#ifndef BASE64_H_
#define BASE64_H_

#include <string>
using namespace std;


string base64_encode(char const* , int len);
string base64_decode(string const& s);

#endif /* BASE64_H_ */
