/*
 * datachange.h
 *  Created on: 2014-2-26
 *      Author: tanziwen
 */

#ifndef DATACHANGE_H_
#define DATACHANGE_H_

#include<mysql++.h>
#include<string>
#include "../json/json.h"
#include "Log.h"
#include "typedef.h"
using namespace std;

Json::Value ResToJson(mysqlpp::StoreQueryResult res);
//json to step date
string JsonToDate_step(const string &user_id, const string jsondate);
//json to sleep date
string JsonToDate_sleep(const string &user_id,const string &jsondate);
//json to position date
string JsonToDate_detail(const string &user_id,const string &jsondate);
//data change social tema
string ResKey(mysqlpp::StoreQueryResult res,const string key);
//date change target setting
string JsonToSetting(const string &user_id,const string &jsondate);

#endif /* DATACHANGE_H_ */
