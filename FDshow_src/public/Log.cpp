/*
 * 	Log.cpp
 *  Created on: 2014-2-26
 *      Author: tanziwen
 *      Description  : describle functions of log
 */
#include "Log.h"
#include "profile.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>
#include <stdarg.h>
#include <sstream>
#include "Synch.h"
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <errno.h>

using namespace std;

#define LOCK_FILE_MODE  S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP

//log lock
CThread_Mutex LogMutex;

CLog *CLog::m_pLog = NULL;

CLog::CLog(void) {
	m_pszPath = NULL;
	m_pszFName = NULL;
	bFinished = false;
	m_LogLevel = LOG_LEVEL_DEBUG; //set log level debug,the profile can modified
	m_iNum = 0;
	bMultiProcess = false;
	m_LockFd = 0;
	m_iMaxSize = MAX_SIZE;
	pthread_mutex_init(&this->m_logmutex, NULL);
	pthread_mutex_init(&this->m_lstmutex, NULL);
	pthread_cond_init(&this->m_lstcond, NULL);
}

CLog::~CLog(void) {
	if (fp.is_open()) {
		fp.flush();
		fp.close();
	}
	if (m_pszPath) {
		delete[] m_pszPath;
		m_pszPath = NULL;
	}
	if (m_pszFName) {
		delete[] m_pszFName;
		m_pszFName = NULL;
	}
	if (bMultiProcess)
		if (m_LockFd > 0)
			::close(m_LockFd);

	pthread_mutex_destroy(&this->m_logmutex);
	pthread_mutex_destroy(&this->m_lstmutex);
	pthread_cond_destroy(&this->m_lstcond);
}

bool CLog::initialize(const char *pszPath, const char *pszFName) {
	if (NULL == pszPath || NULL == pszFName) {
		return false;
	}

	DIR *pDir;
	string fpName;

	//if path was seted,return
	if (m_pszPath != NULL) {
		return true;
	}

	//open log directory,if not exist create it

	//if the action failed ,the reason maybe you not have the permision,or the directory not exisit
	pDir = opendir(pszPath);
	if (pDir) {
		/* Directory exists. */
		closedir(pDir);
	} else if (ENOENT == errno) {
		/* if Directory not exist. then create the directory*/
		if (CreateDir(pszPath) != 0) {
			perror("create directory error!");
			return false;
		}
	} else {
		/* opendir() failed for some other reason. */
		perror("opendir");
		return false;
	}

	//save useable path
	m_pszPath = new char[strlen(pszPath) + 1];
	strcpy(m_pszPath, pszPath);
	//save the name which is the first part of log name
	m_pszFName = new char[strlen(pszFName) + 1];
	strcpy(m_pszFName, pszFName);

	//directory + log name
	m_strDir = m_pszPath;
	m_strDir += "/";
	m_strDir += m_pszFName;
	m_strDir += "_";

	// support mulit-thread 
	if (bMultiProcess) {
		string strLockFile(m_strDir);
		string::size_type nPos = strLockFile.rfind("/");
		if (string::npos != nPos) {
			strLockFile.insert(nPos + 1, ".");
		}
		strLockFile += "lock";

		int fd = -1;
		if ((fd = ::open(strLockFile.c_str(), O_RDWR | O_CREAT, LOCK_FILE_MODE))
				< 0) {
			return -1;
		}
		struct flock flk;
		flk.l_type = F_WRLCK; //F_RDLCK, F_WRLCK, or F_UNLCK
		flk.l_whence = SEEK_SET; //SEEK_SET, SEEK_CUR, SEEK_END
		flk.l_start = 0; //starting offset relative to l_whence
		flk.l_len = 0; //len == 0 means "until end of file"
		flk.l_pid = getpid(); //Process ID of the process holding the

		//////////////////////////////////////////
		if (fcntl(fd, F_SETLKW, &flk) < 0) { //F_SETLKW
			::close(fd);
			return -1;
		}

		m_LockFd = fd;
	}

	return true;
}

void CLog::finalize(void) {
	CMutex_Guard<CThread_Mutex> mon(LogMutex);
	if (m_pLog) {
		delete m_pLog;
		m_pLog = NULL;
	}
}

CLog *CLog::getInstance(void) {
	if (NULL == m_pLog) {
		CMutex_Guard<CThread_Mutex> mon(LogMutex);
		if (NULL == m_pLog) {
			m_pLog = new CLog;
		}
	}

	return m_pLog;
}

ofstream& CLog::operator<<(const char *buf) {
	//if initianlize failed,external-calling return refrence with no data-write
	if (NULL == m_pszFName || NULL == m_pszPath || NULL == buf) {
		return fp;
	}
	ostringstream ostrNum;
	bool bChg = false;
	string ffile; //name of log
	string fpath; //path of log
	string strd; //date
	string flog = ".log";

	this->getDate(strd);

	struct stat fstatus;
	fpath = m_strDir;
	fpath += strd;
	fpath += "_";

	//initialize,if the index is 0
	if (0 == m_iNum) {
		//find the vaild file
		for (;;) {
			++m_iNum;
			ostrNum.str("");
			ostrNum << m_iNum;

			ffile = fpath;
			ffile += ostrNum.str();
			ffile += flog;

			//if the file not exist,return
			if (stat(ffile.c_str(), &fstatus) != 0) {
				break;
			} else { //compare the file size
				if (fstatus.st_size < m_iMaxSize) {
					break;
				}
			}
		}
		//save the date ,compared with date changed		
		m_strDate = strd;
	} else { //compose the path
		//if date has change ,need initialize
		if (strd != m_strDate) {
			m_iNum = 1;
			m_strDate = strd;
			if (fp.is_open()) {
				fp.close();
			}
		}

		ostrNum.str("");
		ostrNum << m_iNum;
		ffile = fpath;
		ffile += ostrNum.str();
		ffile += flog;
	}

	// multi-thread
	if (!bMultiProcess) {
		//file not exist
		if (stat(ffile.c_str(), &fstatus) != 0) {
			//if file is delete by mistake,examined
			if (fp.is_open()) {
				fp.close();
			}
			//maybe the file not exist,create it ,need power
			fp.open(ffile.c_str(), ios_base::out | ios_base::app);
			//if open file failed,return file-stream without data write
			if (!fp.is_open()) {
				return fp;
			}
		} else { //file exsit
			if (!fp.is_open()) {
				fp.open(ffile.c_str(), ios_base::out | ios_base::app);
				if (!fp.is_open()) {
					return fp;
				}
			}
			//size compare
			if (fstatus.st_size >= m_iMaxSize) {
				bChg = true;
				++m_iNum;
			}
		}
		fp << buf << endl;
		//file changed,close the file-stream
		if (bChg) {
			fp.flush();
			fp.close();
		}
	} else { // support muli-thread
		lock(m_LockFd);
		//maybe the file not exist,create it ,need power
		fp.open(ffile.c_str(), ios_base::out | ios_base::app);
		if (!fp.is_open())
			return fp;

		//check the size is permit
		if (fp.tellp() >= m_iMaxSize)
			++m_iNum;

		fp << buf << endl;
		fp.flush();
		fp.close();
		unlock(m_LockFd);
	}
	return fp;
}

int CLog::getDate(string &strDate) {
	timeval tv;
	memset(&tv, 0, sizeof(tv));

	struct tm tmNow;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &tmNow);

	char tmbuf[10] = { 0 };
	sprintf(tmbuf, "%4d%02d%02d", tmNow.tm_year + 1900, tmNow.tm_mon + 1,
			tmNow.tm_mday);

	strDate = tmbuf;

	return 0;
}

int CLog::getLogLevel(void) {
	return m_LogLevel;
}

void CLog::setLogLevel(int level) {
	pthread_mutex_lock(&this->m_logmutex);
	//legal log level intput,or set it back
	if (LOG_LEVEL_DEBUG == level || LOG_LEVEL_INFO == level
			|| LOG_LEVEL_WARN == level || LOG_LEVEL_ERROR == level) {
		m_LogLevel = level;
	}
	pthread_mutex_unlock(&this->m_logmutex);
}

void CLog::setLogLevel(const char *pszLvl) {
	pthread_mutex_lock(&this->m_logmutex);

	//legal log level intput,or set it back
	if (strcasecmp(pszLvl, "debug") == 0) {
		m_LogLevel = LOG_LEVEL_DEBUG;
	} else if (strcasecmp(pszLvl, "info") == 0) {
		m_LogLevel = LOG_LEVEL_INFO;
	} else if (strcasecmp(pszLvl, "warning") == 0) {
		m_LogLevel = LOG_LEVEL_WARN;
	} else if (strcasecmp(pszLvl, "error") == 0) {
		m_LogLevel = LOG_LEVEL_ERROR;
	}

	pthread_mutex_unlock(&this->m_logmutex);
}

int CLog::log(int log_priority, int nline, const char *file,
		const char *format_str, ...) {
	string strdata;
	pthread_t pthrid;
	int iLogLevel = getLogLevel();

	//if log level was seted,write log
	if ((iLogLevel <= log_priority)) {
		//write date
		timeval tv;
		bzero(&tv, sizeof(tv));

		struct tm tmCur;

		gettimeofday(&tv, NULL);
		localtime_r(&tv.tv_sec, &tmCur);

		char tmbuf[30] = { 0 };
		sprintf(tmbuf, "%d-%02d-%02d %02d:%02d:%02d %03ld ",
				tmCur.tm_year + 1900, tmCur.tm_mon + 1, tmCur.tm_mday,
				tmCur.tm_hour, tmCur.tm_min, tmCur.tm_sec, tv.tv_usec / 1000);

		strdata = tmbuf;

		char tstr[11] = { 0 };
		pthrid = pthread_self();
		sprintf(tstr, "%u", (unsigned int) pthrid);
		strdata += "[";
		strdata += tstr;
		strdata += "] ";

		//log level
		switch (log_priority) {
		case 1: {
			strdata += "DEBUG [";
			break;
		}
		case 2: {
			strdata += "INFO  [";
			break;
		}
		case 4: {
			strdata += "WARN  [";
			break;
		}
		case 8: {
			strdata += "ERROR [";
			break;
		}
		default: {
			//error parm
			break;
		}
		}

		//write the line number and filename into log
		strdata += file;
		memset(tstr, 0, 11);
		sprintf(tstr, "%4d", nline);
		strdata += tstr;
		strdata += "] ";

		//write the content ,must be less than 2k
		char dbuf[MAX_LOG_BUF_SIZE] = { 0 };

		va_list ap;
		va_start(ap, format_str);
		int vs = vsnprintf(dbuf, MAX_LOG_BUF_SIZE - 1, format_str, ap);
		va_end(ap);
		if (vs < 0) {
			strdata += "log output error";
		} else {
			strdata += dbuf;
		}
		pthread_mutex_lock(&this->m_lstmutex);
		m_lstBuf.push_back(strdata);
		pthread_cond_signal(&this->m_lstcond);
		pthread_mutex_unlock(&this->m_lstmutex);
		return vs;
	}
	return -1;
}

int CLog::svc(void) {
	string strBuf;
	CLog *plog = CLog::getInstance();
	while (!bFinished) {
		pthread_mutex_lock(&this->m_lstmutex);
		while (m_lstBuf.empty()) {
			pthread_cond_wait(&this->m_lstcond, &this->m_lstmutex);
		}
		strBuf = m_lstBuf.front();
		m_lstBuf.pop_front();
		pthread_mutex_unlock(&this->m_lstmutex);

		if (strBuf.empty()) {
			break;
		}
		//write log using the redefined function <<
		//cout<<"thread:"<<pthread_self()<<" done the work!"<<endl;
		*plog << strBuf.c_str();
	}
	return 0;
}

//function:create muli-thread
int CLog::open(int iNum) {
	pthread_t thr_id;
	int thr_num = 0;

	for (int i = 0; i < iNum; ++i) {
		if (pthread_create(&thr_id, NULL, &CLog::svc_run, (void *) this) == 0) {
			m_lstThrId.push_back(thr_id);
			++thr_num;
		}
		sleep(1);
	}
	if (0 == thr_num) {
		return -1;
	}
	return 0;
}

int CLog::close() {
	string strTmp = "";
	list<pthread_t>::iterator iter;
	bFinished = true;
	for (iter = m_lstThrId.begin(); iter != m_lstThrId.end(); ++iter) {
		pthread_mutex_lock(&this->m_lstmutex);
		m_lstBuf.push_back(strTmp);
		pthread_cond_signal(&this->m_lstcond);
		pthread_mutex_unlock(&this->m_lstmutex);
	}
	for (iter = m_lstThrId.begin(); iter != m_lstThrId.end(); ++iter) {
		pthread_join(*iter, NULL);
	}

	return 0;
}

void *CLog::svc_run(void *param) {
	//format parm
	CLog *t = (CLog *) param;
	//callback the real function
	t->svc();
	return NULL;
}

int CLog::lock(int fd) {
	struct flock flk;
	flk.l_type = F_WRLCK; //F_RDLCK, F_WRLCK, or F_UNLCK
	flk.l_whence = SEEK_SET; //SEEK_SET, SEEK_CUR, SEEK_END
	flk.l_start = 0; //starting offset relative to l_whence
	flk.l_len = 0; //len == 0 means "until end of file"
	flk.l_pid = getpid(); //Process ID of the process holding the

	if (fcntl(fd, F_SETLKW, &flk) < 0) { //F_SETLKW
		::close(fd);
		return -1;
	}
	return 0;
}

int CLog::unlock(int fd) {
	struct flock flk;
	flk.l_type = F_UNLCK; //F_RDLCK, F_WRLCK, or F_UNLCK
	flk.l_whence = SEEK_SET; //SEEK_SET, SEEK_CUR, SEEK_END
	flk.l_start = 0; //starting offset relative to l_whence
	flk.l_len = 0; //len == 0 means "until end of file"
	flk.l_pid = getpid(); //Process ID of the process holding the

	if (fcntl(fd, F_SETLKW, &flk) < 0) { //F_SETLKW
		::close(fd);
		return -1;
	}
	return 0;
}
