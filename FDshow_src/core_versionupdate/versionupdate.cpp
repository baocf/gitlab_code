#include "versionupdate.h"
#include <sstream>
#include <mysql++.h>
#include <exceptions.h>
#include "../public/Log.h"
#include "../public/typedef.h"
#include "../public/DBConnectPool.h"
#include "../public/datachange.h"
#include "../public/protocol.h"
#include "../json/json.h"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include "fcgi_stdio.h"

using namespace std;

#define RECV_BUF_SIZE 1024
#define memsize 1000

pthread_mutex_t FDVersion::m_Mutex = PTHREAD_MUTEX_INITIALIZER;

FDVersion::FDVersion(){
	initialize();
}

int FDVersion::initialize() {
	FCGX_InitRequest(&m_Request, 0, 0);
	retcode = 901;
	return 0;
}

string FDVersion::getEnv(const string& param) {
	char* val = FCGX_GetParam(param.c_str(), m_Request.envp);
	if (val == NULL) {
		return "";
	}
	return val;
}

void FDVersion::run() {
	while (!m_Shutdown) {
		Json::Value retstring,parmString;
		pthread_mutex_lock(&m_Mutex);
		int ret = FCGX_Accept_r(&m_Request);
		pthread_mutex_unlock(&m_Mutex);
		if (ret < 0) {
			SVC_LOG((LM_ERROR, " FCG_RUN:FCGX_Accept_r failed ret=%d",ret));
			continue;
		}
		string value = getEnv("REQUEST_METHOD");
		if (0 == strcasecmp(value.c_str(), "post")) { //deal the method post
			char buf[RECV_BUF_SIZE + 1] = { 0 };
			long length = strtoul(getEnv("CONTENT_LENGTH").c_str(), NULL, 10);
			if (length == 0) { //if length equal 0 :maybe error
				SVC_LOG((LM_ERROR, " FCG_RUN:Http header Content-Length invalid:content-length=%d", length));
				retcode = 902;
			} else {
				FCGX_GetStr(buf, sizeof(buf), m_Request.in);
				SVC_LOG((LM_INFO, " FCG_RUN:msg=begin to read Http content:length=%u|", length));
				//parse the content
				parseQueryString(buf);//parse the string and resort to map
				if (0 == m_UrlParams.size()) {
					SVC_LOG((LM_INFO, " FCG_RUN: msg=parse parm erron!"));
					continue;
				}

				if(!HandleSql(parmString)){//check parameters and operator the database
					SVC_LOG((LM_INFO, " FCG_RUN:parameters error or operator database error!"));
				}
			}
		} else {
			retcode = 905;
		}
		//return to client
		Response(retstring,parmString);
	}
}

void FDVersion::parseQueryString(const string& str) {
	size_t begin = 0, pos;
	m_UrlParams.clear();
	do {
		pos = str.find('=', begin);
		if (pos != string::npos) {
			string key = str.substr(begin, pos - begin);
			begin = pos + 1;
			pos = str.find('&', begin);
			if (pos != string::npos) {
				m_UrlParams[key] = URLDecode(str.substr(begin, pos - begin));
				begin = pos + 1;
			} else {
				m_UrlParams[key] = URLDecode(str.substr(begin));
			}
		}
	} while (pos != string::npos);
}
bool FDVersion::HandleSql(Json::Value & parmString) {
	string options,version,str_os,userid,select;
	StringMap::iterator pos;
	ConnPool * mypool = ConnPool::GetInstance();
	mysqlpp::Connection *conn = mypool->GetConnection();
	mysqlpp::Query query(conn,true,NULL);
	//check parameters
	appversion.clear();
	btversion.clear();
	info.clear();
	pos = m_UrlParams.find("userid");
	if (pos != m_UrlParams.end()) {
		userid = pos->second;
	}
	pos = m_UrlParams.find("options");
	if (pos != m_UrlParams.end()) {
		options = pos->second;
	}
	pos = m_UrlParams.find("select");
	if (pos != m_UrlParams.end()) {
		select = pos->second;
	}

	if (userid.empty()||options.empty()||select.empty()) {
		retcode = 902;
		return false;
	}
	char * sqlstate = new char[memsize];
	if (NULL == sqlstate) {
		SVC_LOG((LM_ERROR, "HandleSql:malloc memery failed! "));
		retcode = 902; //server operator error
		return false;
	}
	memset(sqlstate, '\0', memsize);
	if(atoi(select.c_str()) == 1){ //app update
		pos = m_UrlParams.find("version");
		if (pos != m_UrlParams.end()) {
			version = pos->second;
		}
		if(version.empty()){
			retcode = 902;
			goto END;
		}
		snprintf(sqlstate, memsize,"select vch_version as version,vch_url as url from t_version_app where date_modify = (select max(date_modify) from t_version_app where int_os = %d) and   int_os = %d;",atoi(options.c_str()),atoi(options.c_str()));//format string
	}else if(atoi(select.c_str()) == 2){//buleteeth update
		pos = m_UrlParams.find("version");
		if (pos != m_UrlParams.end()) {
			version = pos->second;
		}
		if(version.empty()){
			retcode = 902;
			goto END;
		}
		snprintf(sqlstate, memsize,"select vch_version as version,vch_url as url from t_version_blueteeth where date_modify = (select max(date_modify) from t_version_blueteeth);");//format string)
	}else if(atoi(select.c_str()) == 3){// get the version of app and blueteeth
		try{
			snprintf(sqlstate, memsize,"select vch_version as appversion from t_version_app where date_modify = (select max(date_modify) from t_version_app where int_os = %d) and   int_os = %d;",atoi(options.c_str()),atoi(options.c_str()));//format string
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult AppVersion = query.store();
			if (!AppVersion.empty()) {
				appversion = AppVersion[0]["appversion"].c_str();
			}
			memset(sqlstate,'\0',memsize);
			snprintf(sqlstate, memsize,"select vch_version as btversion from t_version_blueteeth where date_modify = (select max(date_modify) from t_version_blueteeth);");//format string)
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult BtVersion = query.store();
			if (!BtVersion.empty()) {
				btversion = BtVersion[0]["btversion"].c_str();
			}
			memset(sqlstate,'\0',memsize);
			snprintf(sqlstate, memsize,"select vch_version as uiversion from t_version_ui where date_modify = (select max(date_modify) from t_version_ui);");//format string)
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult UIVersion = query.store();
			if (!UIVersion.empty()) {
				uiversion = UIVersion[0]["uiversion"].c_str();
			}
			retcode = 901;
		}catch(mysqlpp::Exception&){
			retcode = 904;
		}
		goto END;
	}else if(atoi(select.c_str())== 4){
		//UI update
		pos = m_UrlParams.find("version");
		if (pos != m_UrlParams.end()) {
			version = pos->second;
		}
		if(version.empty()){
			retcode = 902;
			goto END;
		}
		snprintf(sqlstate, memsize,"select vch_version as version,vch_url as url from t_version_ui where date_modify = (select max(date_modify) from t_version_ui)");//format string
	}

	SVC_LOG((LM_INFO, " FCG_RUN:sql=%s", sqlstate));
	try {
		query.reset();
		query.exec("set names utf8");
		query = conn->query(sqlstate);
		mysqlpp::StoreQueryResult version_res = query.store();
		if (!version_res.empty()) {
			if(strcmp(version.c_str(),version_res[0]["version"].c_str()) == 0){
				retcode = 901; //the client version is the lastest
			}else{
				retcode = 903;
				parmString = ResToJson(version_res);
			}
		}else{
			retcode = 902;
		}
	} catch (mysqlpp::Exception&) {
		SVC_LOG((LM_ERROR, " HandleSql:ERROR=%s", conn->error()));
		retcode =904;
	}
	END:
	if(NULL!=conn){
		mypool->ReleaseConnection(conn);
	}
	if(NULL != sqlstate){
		delete sqlstate;
		sqlstate = NULL;
	}
	if(retcode == 901 || retcode == 903)
		return true;
	else
		return false;
}

void FDVersion::Response(Json::Value retstring,Json::Value parmString){
	ostringstream oss;
	switch(retcode){
	case 901:
		{info = "the version is the latest !";break;}
	case 902:
		{info = "parameter error.";break;}
	case 903:
		{info = "the version is not the latest!";break;}
	case 904:
		{info = "server error!";break;}
	case 905:
		{info = "method error!";break;}
	default:
		{info = "";break;}
	}

	retstring["retcode"] = retcode;
	retstring["info"] = info;
	retstring["version"] = parmString;
	retstring["btverseion"] = btversion;
	retstring["appversion"] = appversion;
	retstring["uiversion"] = uiversion;
	string str_ret = retstring.toStyledString();
	oss << "Content-type:text/html" << "\r\n\r\n"
		<< str_ret
		<< "\r\n";
	string rsp = oss.str();
	oss.str("");
	rsp.insert(0, oss.str());
	FCGX_PutStr(rsp.c_str(), rsp.size(), m_Request.out);
	FCGX_FFlush(m_Request.out);
	FCGX_Finish_r(&m_Request);
}
