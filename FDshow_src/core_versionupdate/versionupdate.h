
#ifndef FCGI_HANDLER_H
#define FCGI_HANDLER_H

#include <string>
#include <map>
#include <pthread.h>
#include <mysql++.h>
#include "../public/ThreadBase.h"
#include "fcgi_stdio.h"
#include "fcgiapp.h"
#include "fcgio.h"
#include "../json/json.h"
using namespace std;


class FDVersion : public CThreadBase {
    public:
        void run();
        int initialize();
        FDVersion();
    private:
        std::string getEnv(const std::string& param);
        std::string getVal(const std::string& param);
        void parseQueryString(const std::string& qs);
        bool HandleSql(Json::Value&retstring);
        void Response(Json::Value retstring,Json::Value  parmString);
        int CheckLogin(mysqlpp::Connection *conn,char * sqlstate,string userid,string loginid);//check login
    private:
        std::map<std::string, std::string> m_UrlParams;
        FCGX_Request m_Request;
        int retcode;
        string info;
        string btversion;
        string appversion;
        string uiversion;
    private:
        static pthread_mutex_t m_Mutex;
};

#endif
