#include "maintain.h"
#include "../public/Log.h"
#include "../public/typedef.h"
#include "../public/DBConnectPool.h"
#include "../public/datachange.h"
#include "../public/protocol.h"
#include "../json/json.h"
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <mysql++.h>
#include <exceptions.h>
#include "fcgi_stdio.h"

using namespace std;

#define RECV_BUF_SIZE 1024
#define MEMSIZE 1000

pthread_mutex_t FDProMaintain::m_Mutex = PTHREAD_MUTEX_INITIALIZER;

FDProMaintain::FDProMaintain(){
	//initialize the object
	initialize();
}
int FDProMaintain::initialize() {
	int retcode = 101;	//initialize
	FCGX_InitRequest(&m_Request, 0, 0);
	return 0;
}

string FDProMaintain::getEnv(const string& param) {
	char* val = FCGX_GetParam(param.c_str(), m_Request.envp);
	if (val == NULL) {
		return "";
	}
	return val;
}

void FDProMaintain::run() {

	while (!m_Shutdown) {
		//string product;
		Json::Value product;
		Json::Value retstring;

		int ret = FCGX_Accept_r(&m_Request);
		if (ret < 0) {
			SVC_LOG((LM_ERROR, " FCG_RUN:FCGX_Accept_r failed ret =%d",ret));
			continue;
		}
		string value = getEnv("REQUEST_METHOD");
		if (0 == strcasecmp(value.c_str(), "post")) { //deal the method post
			char buf[RECV_BUF_SIZE + 1] = { 0 };
			long length = strtoul(getEnv("CONTENT_LENGTH").c_str(), NULL, 10);
			if (length == 0) { //if length equal 0 :maybe error
				SVC_LOG((LM_ERROR, " FCG_RUN:Http header Content-Length invalid:content-length=%d", length));
				retcode = 702;
			} else {
				FCGX_GetStr(buf, sizeof(buf), m_Request.in);
				SVC_LOG((LM_INFO, " FCG_RUN:msg=begin to read Http content:length=%u", length));
				SVC_LOG((LM_INFO, " FCG_RUN:parament1:%s", buf));

				//parse the content
				parseQueryString(buf);//parse the string and resort to map
				if (0 == m_UrlParams.size()) {
					SVC_LOG((LM_ERROR, " FCG_RUN: msg=parse parm erron!"));
					continue;
				}
				if(!HandleSql_Post(product)){//check parameters and operator the database
					SVC_LOG((LM_ERROR, " FCG_RUN:parameters error or operator database error!"));
				}
			}
		} else if (0 == strcasecmp(value.c_str(), "get")) {
			string buffer = getEnv("QUERY_STRING");
			SVC_LOG((LM_INFO, " get parameters:%s",buffer.c_str()));
			if (buffer.empty()) {
				SVC_LOG((LM_ERROR, " FCG_RUN:parameters error the string is null!"));
				ret=702;
			}
			parseQueryString(buffer);
			if (!HandleSql_Get(product)) {
				SVC_LOG((LM_ERROR, " FCG_RUN:parameters error or operator database error!"));
			}
		}
		else{
			retcode = 705;
		}
		//return to client
		Response(product,retstring);
	}
}

void FDProMaintain::parseQueryString(const string& str) {
	size_t begin = 0, pos;
	m_UrlParams.clear();
	do {
		pos = str.find('=', begin);
		if (pos != string::npos) {
			string key = str.substr(begin, pos - begin);
			begin = pos + 1;
			pos = str.find('&', begin);
			if (pos != string::npos) {
				m_UrlParams[key] = URLDecode(str.substr(begin, pos - begin));
				begin = pos + 1;
			} else {
				m_UrlParams[key] = URLDecode(str.substr(begin));
			}
		}
	} while (pos != string::npos);
}


bool FDProMaintain::HandleSql_Post(Json::Value &maintain) {
	string options;
	StringMap::iterator pos;
	ConnPool * mypool = ConnPool::GetInstance();
	mysqlpp::Connection *conn = mypool->GetConnection();

	char *sqlstate = new char[MEMSIZE];
	if (NULL == sqlstate) {
		SVC_LOG((LM_ERROR, "HandleSql:malloc memery failed! "));
		retcode = 704; //server operator error
		mypool->ReleaseConnection(conn);
		conn=NULL;
		return false;
	}
	memset(sqlstate, '\0', MEMSIZE);

	pos = m_UrlParams.find("options");
	if (pos != m_UrlParams.end()) {
		options = pos->second;
	}

    if(options.empty()){
		retcode = 702;
		goto END;
	}

	switch(atoi(options.c_str())){
	case 1:{//add product informations
		if(AddProductInfo(conn,sqlstate) != 0){
			SVC_LOG((LM_ERROR,"Add Product infomations error!"));
			goto END;
		}
		break;
	}
	case 2:{//delete product
		if(RemoveProductInfo(conn,sqlstate)!= 0){
			SVC_LOG((LM_ERROR,"Remove Product informations error!"));
			goto END;
		}
		break;
	 }
	case 3:{//update product
		if(ModifyProductInfo(conn, sqlstate) !=0){
			SVC_LOG((LM_ERROR,"Update Product informations error!"));
			goto END;
		}
		break;
	}
	default:
		break;
	}

END:
	if(conn!= NULL){
		mypool->ReleaseConnection(conn);
	}
	if(sqlstate !=NULL)
		delete sqlstate;
	sqlstate = NULL;

	if (retcode % 100 == 1)
		return true;
	else
		return false;
}

bool FDProMaintain::HandleSql_Get(Json::Value &parameter) {
	ConnPool * mypool = ConnPool::GetInstance();
	mysqlpp::Connection *conn = mypool->GetConnection();
	string options, productname, sortname;
	StringMap::iterator pos;
    mysqlpp::Query query(conn,true,NULL);

	pos = m_UrlParams.find("options");
	if (pos != m_UrlParams.end()) {
		options = pos->second;
	}
	if(options.empty()){
		retcode = 702;
		SVC_LOG((LM_ERROR, "parameter error! "));
	}
	char *sqlstate = new char[MEMSIZE];

	if(atoi(options.c_str()) == 1){//find product by productname
	
        pos = m_UrlParams.find("productname");
        if(pos != m_UrlParams.end()){
            productname = pos->second;
        }
		memset(sqlstate,'\0',MEMSIZE);
		snprintf(sqlstate,MEMSIZE,"select vch_productid as productid,vch_productname as productname,int_sortid as sortid,vch_url as url,vch_path as path,vch_note as note,date_modify,int_flag as flag,vch_scheme as scheme from t_product_info where vch_productname = '%s' and flag = 1", productname.c_str());
        try {
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult personal_info = query.store();
			parameter = ResToJson(personal_info);
			SVC_LOG((LM_INFO, "HandleSql:sql operator end! "));
			retcode = 701;
		} catch (mysqlpp::Exception&) {
			SVC_LOG((LM_ERROR, "HandleSql:sql operator failed! "));
			retcode = 704;
		}
	}else if(atoi(options.c_str()) == 2){//find product by sortname
        pos = m_UrlParams.find("sortname");
        if(pos != m_UrlParams.end()){
            sortname = pos->second;
        }
		memset(sqlstate,'\0',MEMSIZE);
		snprintf(sqlstate, MEMSIZE,	"call proc_find_by_sortname('%s')",sortname.c_str());
		try {
			query.reset();
			query.exec("set names utf8");
			query = conn->query(sqlstate);
			mysqlpp::StoreQueryResult PersonalSet = query.store();
			parameter = ResToJson(PersonalSet);
			SVC_LOG((LM_INFO, "HandleSql:sql operator end! "));
			retcode = 701;
		} catch (mysqlpp::Exception&) {
			SVC_LOG((LM_ERROR, "HandleSql:sql operator failed! "));
			retcode = 704;
		}
	}
	if(conn!= NULL){
		mypool->ReleaseConnection(conn);
	}
	if(sqlstate !=NULL)
		delete sqlstate;
	sqlstate = NULL;

	if (retcode % 100 == 1)
		return true;
	else
		return false;
}
int FDProMaintain::AddProductInfo(mysqlpp::Connection *conn,char * sqlstate){
	string productid,productname, sortid,productpath,producturl,productnote,productscheme;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("productid");
	if (pos != m_UrlParams.end()) {
		productid = pos->second;
	}
	//parameters check
	if (productid.empty()){
		retcode = 702;
		return -1;
	}
	memset(sqlstate,'\0',MEMSIZE);

	pos = m_UrlParams.find("productname");
	if (pos != m_UrlParams.end()&&!pos->second.empty()){
		productname = pos->second;
	}
	pos = m_UrlParams.find("sortid");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		sortid = pos->second;
	}
	pos = m_UrlParams.find("productpath");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productpath= pos->second;
	}
	pos = m_UrlParams.find("producturl");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		producturl = pos->second;
	}
	pos = m_UrlParams.find("productnote");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productnote = pos->second;
	}
	pos = m_UrlParams.find("productscheme");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productscheme = pos->second;
	}
	memset(sqlstate,'\0',MEMSIZE);
	snprintf(sqlstate,MEMSIZE,"call proc_add_productinfo('%s','%s','%s','%s','%s','%s','%s')",productid.c_str(),productname.c_str(),sortid.c_str(),productpath.c_str(),producturl.c_str(),productnote.c_str(),productscheme.c_str());
	SVC_LOG((LM_ERROR, "sqlstate:%s",sqlstate));
	try{
		query.reset();
		query.exec("set names utf8");
		if (query.exec(sqlstate)) {
			retcode = 701;
		}
	}catch(mysqlpp::Exception&){
		SVC_LOG((LM_ERROR, " HandleSql_post:ERROR=%s", conn->error()));
		retcode = 704;
		return -1;
	}
	return 0;
}


int FDProMaintain::RemoveProductInfo(mysqlpp::Connection *conn, char *sqlstate)
{
	string productid, sortid, type;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
    if(conn == NULL || sqlstate == NULL){
        retcode = 704;
        return -1;
    }

	pos = m_UrlParams.find("type");
	if (pos != m_UrlParams.end()) {
		type= pos->second;
	}
	if(productid.empty() || type.empty()){
		retcode = 702;
		return -1;
	}
    switch(atoi(type.c_str())){
        case 1:{//delete by productid
            pos = m_UrlParams.find("productid");
            if (pos != m_UrlParams.end()) {
                productid= pos->second;
            }
            memset(sqlstate,'\0',MEMSIZE);
        	snprintf(sqlstate,MEMSIZE,"update t_product_info set int_flag = 0,date_modify = now() where vch_productid= '%s' and int_flag = 1;",productid.c_str());
            try{
    	    	query.reset();
	    	    query.exec("set names utf8");
    		    if (query.exec(sqlstate)) {
	    		    retcode = 701;
    	    	}   
    	    }catch(mysqlpp::Exception&){
	    	    SVC_LOG((LM_ERROR, " HandleSql_post:ERROR=%s", conn->error()));
    	    	retcode = 704;
	    	    return -1;
        	}
            break;
    }
    case 2:{//delete by sortid
        pos = m_UrlParams.find("sortid");
        if(pos != m_UrlParams.end()){
            sortid = pos->second;
        }
        if(sortid.empty()){
            retcode = 702;
        }
	    memset(sqlstate,'\0',MEMSIZE);
    	snprintf(sqlstate,MEMSIZE,"call proc_delete_by_sortid('%s')",sortid.c_str());
        try{
    		query.reset();
	    	query.exec("set names utf8");
		    if (query.exec(sqlstate)) {
			    retcode = 701;
    		}   
	    }catch(mysqlpp::Exception&){
		    SVC_LOG((LM_ERROR, " HandleSql_post:ERROR=%s", conn->error()));
    		retcode = 704;
	    	return -1;
    	}
        break;
    }
    case 3:{//delete by all
	    memset(sqlstate,'\0',MEMSIZE);
    	snprintf(sqlstate,MEMSIZE,"update t_product_info set int_flag = 0,date_modify = now() where int_flag = 1");
        try{
    		query.reset();
	    	query.exec("set names utf8");
		    if (query.exec(sqlstate)) {
			    retcode = 701;
    		}   
	    }catch(mysqlpp::Exception&){
		    SVC_LOG((LM_ERROR, " HandleSql_post:ERROR=%s", conn->error()));
    		retcode = 704;
	    	return -1;
    	}
        break;
        }
    default:{
        break;
        }
    }
    return 0;
}
 
int FDProMaintain::ModifyProductInfo(mysqlpp::Connection *conn,char * sqlstate)
{
	string productid, productname, sortid,productpath,producturl,productnote,productscheme;
	StringMap::iterator pos;
	mysqlpp::Query query(conn,true,NULL);
	pos = m_UrlParams.find("productid");
	if (pos != m_UrlParams.end()) {
		productid = pos->second;
	}

	pos = m_UrlParams.find("productname");
	if (pos != m_UrlParams.end()&&!pos->second.empty()){
		productname = pos->second;
	}
	pos = m_UrlParams.find("sortid");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		sortid = pos->second;
	}
	pos = m_UrlParams.find("productpath");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productpath= pos->second;
	}
	pos = m_UrlParams.find("producturl");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		producturl = pos->second;
	}
	pos = m_UrlParams.find("productnote");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productnote = pos->second;
	}
	pos = m_UrlParams.find("productscheme");
	if (pos != m_UrlParams.end()&&!pos->second.empty()) {
		productscheme = pos->second;
	}
	//parameters check
	if (productid.empty()){
		retcode = 702;
		return -1;
	}
	memset(sqlstate,'\0',MEMSIZE);
	snprintf(sqlstate,MEMSIZE,"call proc_update_productinfo('%s','%s','%s','%s','%s','%s','%s')",productid.c_str(),productname.c_str(),sortid.c_str(),productpath.c_str(),producturl.c_str(),productnote.c_str(),productscheme.c_str());
	SVC_LOG((LM_ERROR, "sqlstate:%s",sqlstate));
	try{
		query.reset();
		query.exec("set names utf8");
		if (query.exec(sqlstate)) {
			retcode = 701;
		}
	}catch(mysqlpp::Exception&){
		SVC_LOG((LM_ERROR, " HandleSql_post:ERROR=%s", conn->error()));
		retcode = 704;
		return -1;
	}
	return 0;

}

void FDProMaintain::Response(Json::Value & product,Json::Value & retstring){
	ostringstream oss;
	switch(retcode){
		case 701:
			{info = "operator success.";break;}
		case 702:
			{info = "parameter error.";break;}
		case 703:
			{info = "reserved!";break;}
		case 704:
			{info = "server error!";break;}
		case 705:
			{info = "protocol method error.";break;}
		default:
			{info = "";break;}
		}
	retstring["retcode"] = retcode;
	retstring["info"] = info;
	retstring["dataset"] = product;
	string str_ret = retstring.toStyledString();
	oss << "Content-type:text/json" << "\r\n\r\n"
		<< str_ret
		<< "\r\n";

	string rsp = oss.str();
	oss.str("");
	rsp.insert(0, oss.str());
	FCGX_PutStr(rsp.c_str(), rsp.size(), m_Request.out);
	SVC_LOG((LM_INFO,"JSON_PERSONAL:%s",rsp.c_str()));
	FCGX_FFlush(m_Request.out);
	FCGX_Finish_r(&m_Request);
}
