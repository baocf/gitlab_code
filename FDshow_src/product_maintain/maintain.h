
#ifndef __PRODUCT_MAINTAIN_H__
#define __PRODUCT_MAINTAIN_H__

#include <string>
#include <map>
#include <pthread.h>
#include <mysql++.h>
#include "../public/ThreadBase.h"
#include "fcgi_stdio.h"
#include "fcgiapp.h"
#include "fcgio.h"
#include "../json/json.h"
using namespace std;

class FDProMaintain : public CThreadBase {
    public:
        void run();
        int initialize();
        FDProMaintain();
    private:
        int valid();
        std::string getEnv(const std::string& param);
        std::string getVal(const std::string& param);
        void parseQueryString(const std::string& qs);
        void Response(Json::Value & product,Json::Value & retstring);
        bool HandleSql_Post(Json::Value & maintain);//composite the sql-statement,and excute it
        bool HandleSql_Get(Json::Value &parameter);
        int AddProductInfo(mysqlpp::Connection *conn,char * sqlstate);
        int RemoveProductInfo(mysqlpp::Connection *conn, char *sqlstate);
        int ModifyProductInfo(mysqlpp::Connection *conn,char * sqlstate);
    private:
        std::map<std::string, std::string> m_UrlParams;
        FCGX_Request m_Request;
		int retcode;	//initialize
		string info;		//information of return
    private:
        static pthread_mutex_t m_Mutex;
};

#endif /* __PRODUCT_MAINTAIN_H__ */
