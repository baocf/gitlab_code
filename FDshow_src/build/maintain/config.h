/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.in by autoheader.  */

/* Define to 1 if you have the `fcgi' library (-lfcgi). */
#define HAVE_LIBFCGI 1

/* Define to 1 if you have the `mysqlpp' library (-lmysqlpp). */
#define HAVE_LIBMYSQLPP 1

/* Define to 1 if you have the `pthread' library (-lpthread). */
#define HAVE_LIBPTHREAD 1

/* Name of package */
#define PACKAGE "maintain"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "BUG-REPORT-ADDRESS"

/* Define to the full name of this package. */
#define PACKAGE_NAME "FULL-PACKAGE-NAME"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "FULL-PACKAGE-NAME VERSION"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "full-package-name"

/* Define to the version of this package. */
#define PACKAGE_VERSION "VERSION"

/* Version number of package */
#define VERSION "1.0"
