
#ifndef FCGI_HANDLER_H
#define FCGI_HANDLER_H


#include "../public/ThreadBase.h"
#include "fcgi_stdio.h"
#include "fcgiapp.h"
#include "fcgio.h"
#include "../json/json.h"
#include <string>
#include <map>
#include <pthread.h>

using namespace std;


class FDImage : public CThreadBase {
    public:
        void run();
        int initialize();
        FDImage();
    private:
        std::string getEnv(const std::string& param,FCGX_Request m_Request);
        std::string getVal(const std::string& param);
        void parseQueryString(const std::string& qs);
    	bool HandleRequest_personal(int &retcode,string userid,string filename);
    private:
        std::map<std::string, std::string> m_UrlParams;
        FCGX_Request m_Request;
		int retcode;	//initialize
		string info;		//information of return
		string imagepath;
		string url;
    private:
        static pthread_mutex_t m_Mutex;
};

#endif
