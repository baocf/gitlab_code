#include "../public/profile.h"
#include "../public/Log.h"
#include "../public/DBConnectPool.h"
#include "../public/datachange.h"
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <mysql++.h>
#include <string.h>
#include <map>
#include <signal.h>
#include "image.h"

using namespace std;
using namespace mysqlpp;

//profile path
#define CONFIG_FILE "/var/www/conf/fupload.conf"
//globe profile-variable
OPTIONS_S OPTION;

bool g_Done = false;

extern "C" void handler(int) {
	g_Done = true;
}
//call back function
static bool parseConfig(Section &sec) {
	const char *name = sec.getName();
	if (!strcmp(name, "database")) {
		OPTION.DbInfo.Host = sec.getString("hostname");
		OPTION.DbInfo.Port = sec.getInteger("port", 0);
		OPTION.DbInfo.DB = sec.getString("dbname");
		OPTION.DbInfo.User = sec.getString("user");
		OPTION.DbInfo.Passwd = sec.getString("passwd");
		OPTION.DbInfo.LinkNum = sec.getInteger("linknum", 0);
	} else if (!strcmp(name, "log")) {
		OPTION.Log_info.LogLevel = sec.getInteger("loglevel", 1);
		OPTION.Log_info.LogName = sec.getString("logname");
		OPTION.Log_info.LogPath = sec.getString("logpath");
		OPTION.Log_info.ThreadNumber = sec.getInteger("threadnumber", 1);
	}else if(!strcmp(name,"customization")){
		OPTION.image.ImagePathName = sec.getString("imagedirectoryname");
	}
	return true;
}

//initialize the server
static bool server_initialize() {
	//initialize the FastCGI variable
	FCGX_Init();
	//analyze the profile 
	if (!parseProfile(CONFIG_FILE, parseConfig)) {
		return false;
	}

	//initialize the log
	CLog *pLog = CLog::getInstance();
	if (!pLog->initialize((OPTION.Log_info.LogPath).c_str(),(OPTION.Log_info.LogName).c_str())) {
		pLog->finalize();
		return false;
	}
	//set level for log
	pLog->setLogLevel(OPTION.Log_info.LogLevel);

	//set options for multithread support
	pLog->setMulithtread(OPTION.Log_info.MultiProcess);

	//start the log multithread
	if (pLog->open(OPTION.Log_info.ThreadNumber) < 0) {
		pLog->finalize();
		return false;
	}
	//initialize the DB link pool
	if (!ConnPool::GetInstance()->InitPool(OPTION.DbInfo.DB, OPTION.DbInfo.Host,
			OPTION.DbInfo.User, OPTION.DbInfo.Passwd, OPTION.DbInfo.Port,
			OPTION.DbInfo.LinkNum)) {
		SVC_LOG((LM_ERROR,"Data-Link pool initialize failed!"));
		return false;
	}
	return true;
}

int main() {

	signal(SIGPIPE, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	if (signal(SIGINT, handler) == SIG_ERR) {
		exit(1);
	}
	if (!server_initialize()) {
		return -1;
	};
	SVC_LOG((LM_INFO,"Server initialize sucessed!"));
	std::vector<FDImage*> workers;
	//think for support multithread later
	for (int i = 0; i < 4; i++) {
		FDImage* fhp = new FDImage;
		fhp->start();
		workers.push_back(fhp);
	}
	while (!g_Done)
		sleep(1);

	for (vector<FDImage*>::iterator it = workers.begin();it < workers.end(); it++) {
		(*it)->shutdown();
		delete (*it);
	}
	ConnPool::GetInstance()->DestoryConnPool();
	CLog::getInstance()->close();
	return 0;
}

