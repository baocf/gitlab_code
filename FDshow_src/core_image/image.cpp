#include "image.h"
#include "../public/Log.h"
#include "../public/typedef.h"
#include "../public/DBConnectPool.h"
#include "../public/datachange.h"
#include "../json/json.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include <sstream>
#include <mysql++.h>
#include <exceptions.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <errno.h>
#include <dirent.h>
#include <sys/stat.h>
#include "FCgiIO.h"
#include "fcgi_stdio.h"
#include "../public/profile.h"


using namespace std;
using namespace cgicc;

#define memsize 1000
#define picname 100

extern OPTIONS_S OPTION;

pthread_mutex_t FDImage::m_Mutex = PTHREAD_MUTEX_INITIALIZER;

FDImage::FDImage(){
	//initialize the objects;
	initialize();
}

int FDImage::initialize() {
    retcode = 801;
	FCGX_InitRequest(&m_Request, 0, 0);
	DIR *pDir;
	imagepath.append("/var/www/");
	imagepath.append(OPTION.image.ImagePathName);
	//if path was seted,return
	if (imagepath.c_str() != NULL) {
		return true;
	}
	//if the action failed ,the reason maybe you not have the permision,or the directory not exisit
	pDir = opendir(imagepath.c_str());
	if (pDir) {
		/* Directory exists. */
		closedir(pDir);
	} else if (ENOENT == errno) {
		/* if Directory not exist. then create the directory*/
		if (CreateDir(imagepath.c_str()) != 0) {
			perror("create directory error!");
			return false;
		}
	} else {
		/* opendir() failed for some other reason. */
		perror("opendir");
		return false;
	}
	return 0;

}

string FDImage::getEnv(const string& param,FCGX_Request m_Request) {
	char* val = FCGX_GetParam(param.c_str(), m_Request.envp);
	if (val == NULL) {
		return "";
	}
	return val;
}

void FDImage::run() {

	while (!m_Shutdown) {
		ostringstream oss;
		Json::Value retstring;
		string options,filename,userid;

		pthread_mutex_lock(&m_Mutex);
		int ret = FCGX_Accept_r(&m_Request);
		pthread_mutex_unlock(&m_Mutex);

		if(ret != 0){
			break;
		}
		FCgiIO IO(m_Request);
		Cgicc fcgi(&IO);
		
		//options extend
		const_form_iterator option = fcgi.getElement("options");
		if(option!=(*fcgi).end()&&!option->isEmpty()){
			options = option->getValue();
		}
		const_form_iterator usr = fcgi.getElement("userid");
		if(usr!=(*fcgi).end()&&!usr->isEmpty()){
			userid = usr->getValue();
		}
		const_form_iterator name = fcgi.getElement("filename");
		if ( name!= (*fcgi).end() && !name->isEmpty()) {
			filename = name->getValue();
		}

		if(options.empty()||userid.empty()||filename.empty()){
			retcode = 802;
			SVC_LOG((LM_ERROR, "parm error!"));
			goto END;
		}

		switch(atoi(options.c_str())){
			case 1:{
				 	char * pname = new char[picname];
					if(NULL == pname){
						retcode = 804;
						SVC_LOG((LM_ERROR,"can't allocate memory !"));
						goto END;
					}
					memset(pname,'\0',picname);
					snprintf(pname,picname,"%s/%s_%s",imagepath.c_str(),userid.c_str(),filename.c_str());
					
					cgicc::file_iterator fileIter = fcgi.getFile("file");
					if(fileIter == fcgi.getFiles().end())
					{
						retcode = 803;
						SVC_LOG((LM_ERROR,"GETFILES ERROR"));
						goto END;
					}
					std::ofstream outfile(pname);

					fileIter->writeToStream(outfile);
					memset(pname,'\0',picname);
					snprintf(pname,picname,"/%s/%s_%s",OPTION.image.ImagePathName.c_str(),userid.c_str(),filename.c_str());
					if(!HandleRequest_personal(retcode,userid,pname)){
						retcode = 804;
						SVC_LOG((LM_ERROR,"handle sql error!"));
						goto END;
					}
					//operator the database;
					if(NULL!=pname){
						delete pname;
					}
					break;
			}
			case 2:
				break;
			default:
				break;
		}


	END:
		//return to client
		switch(retcode){
		case 801:
			{info = "operator success.";break;}
		case 802:
			{info = "parameter error.";break;}
		case 803:
			{info = "file is empty!";break;}
		case 804:
			{info = "server error.";break;}
		case 810:
			{info = "you have logined at other mobil";break;}
		default:
			{info = "";break;}
		}
		retstring["retcode"] = retcode;
		retstring["info"] = info;
		retstring["url"] = url;
		string str_ret = retstring.toStyledString();
		oss << "Content-type:text/json" << "\r\n\r\n"
			<< str_ret 
			<< "\r\n";
		string rsp = oss.str();
		oss.str("");
		rsp.insert(0, oss.str());
		FCGX_PutStr(rsp.c_str(), rsp.size(), m_Request.out);
		FCGX_FFlush(m_Request.out);
		FCGX_Finish_r(&m_Request);
	}
}

bool FDImage::HandleRequest_personal(int &retcode,string userid,string filename) {
	ConnPool * mypool = ConnPool::GetInstance();
	mysqlpp::Connection *conn = mypool->GetConnection();
	mysqlpp::Query query(conn,true,NULL);

	char*sqlstate = new char[memsize];
	if(NULL == sqlstate){
		retcode = 804;
		return false;
	}
	memset(sqlstate,'\0',memsize);
	snprintf(sqlstate,memsize,"update t_personal_info set vch_url = '%s' where vch_userid = '%s' ;",filename.c_str(),userid.c_str());
	SVC_LOG((LM_INFO,"SQLSTATE:%s",sqlstate));
	try{
		query.reset();
		if(!query.exec(sqlstate)){
			retcode = 804;
			SVC_LOG((LM_ERROR,"SERVER ERROR:%s!!",conn->error()));
			return false;
		}

	}catch(...){
		retcode = 804;
		SVC_LOG((LM_ERROR, "server error:%s!",conn->error()));
	}
	if(conn!= NULL){
		mypool->ReleaseConnection(conn);
	}
	if(sqlstate !=NULL){
		delete sqlstate;
	}
	url = filename;
	return true;
}
